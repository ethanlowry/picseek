//
//  SolvedViewController.h
//  picseek
//
//  Created by Ethan Lowry on 9/25/12.
//  Copyright (c) 2012 Ethan Lowry. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SolvedViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *solutionPhoto;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) NSString *elapsedTime;
@property (weak, nonatomic) UIImage *solutionImage;
@property (weak, nonatomic) NSString *keyString;
@property (strong, nonatomic) id hunt;

@property (weak, nonatomic) IBOutlet UIButton *btnSeeWinners;
@property (weak, nonatomic) IBOutlet UIButton *btnReset;

@property (nonatomic) BOOL showHomeButton;

- (IBAction)resetBtnClicked:(id)sender;

@end
