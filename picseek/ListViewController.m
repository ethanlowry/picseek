//
//  ListViewController.m
//  picseek
//
//  Created by Ethan Lowry on 9/7/12.
//  Copyright (c) 2012 Ethan Lowry. All rights reserved.
//

#import <CoreData/CoreData.h>
#import <Parse/Parse.h>
#import "AppDelegate.h"
#import "ListViewController.h"
#import "HuntViewController.h"
#import "Hunt.h"
#import "Helper.h"
#import "WinnersViewController.h"


@interface ListViewController ()
- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath;
@end

@implementation ListViewController

@synthesize locationManager;
@synthesize currentLocation;
@synthesize managedObjectContext;
@synthesize fetchedResultsController = _fetchedResultsController;

NSMutableArray *hunts;
NSMutableArray *pfHunts;

BOOL editFromSwipe = NO;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"tealnav"] forBarMetrics:UIBarMetricsDefault];
    
    // currently no limit on how many fetched objects will be return. will create performance issues with lots of hunts.
    NSError *error;
	if (![[self fetchedResultsController] performFetch:&error]) {
		// Update to handle the error appropriately.
		NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
		exit(-1);  // Fail
	}
    
    currentLocation = nil;
    locationManager.delegate = self;

    self.title = @"your hunts";
    
    UIBarButtonItem *homeButton = [[UIBarButtonItem alloc] initWithTitle:@"home" style:UIBarButtonItemStyleBordered target:self action:@selector(goHome:)];
    self.navigationItem.leftBarButtonItem = homeButton;
    
    self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    hunts = [[_fetchedResultsController fetchedObjects] copy];
    
    //grab the winner counts from parse to make sure they are in sync
    //may be a good idea to test for changes before running the query
    [self runQuery];
}

- (void)populateWinnerCount {
    // walk through fetchedObjects. add a winnerCount to each one.
    // in the configureCell method only show a winner count if pfHunts != nil
    if (pfHunts) {
        for (int i = 0; i < [[_fetchedResultsController fetchedObjects] count]; i++) {
            Hunt *thisHunt = [[_fetchedResultsController fetchedObjects] objectAtIndex:i];
            
            int index = [pfHunts indexOfObjectPassingTest:^(id obj, NSUInteger idx, BOOL *stop) {
                *stop = ([[obj valueForKey:@"huntID"] isEqual:thisHunt.huntID]);
                return *stop;
            }];
            if (index != NSNotFound){
                //only need to update the managed object if the winnercounts don't match
                int pfWinnerCount = [[[pfHunts objectAtIndex:index] valueForKey:@"winnerCount"] intValue];
                int huntWinnerCount = [[thisHunt valueForKey:@"winnerCount"] intValue];
                if (pfWinnerCount != huntWinnerCount) {
                    [thisHunt setValue:[NSNumber numberWithInt:pfWinnerCount] forKey:@"winnerCount"];
                    NSError *error;
                    if (![managedObjectContext save:&error]) {
                        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
                    }
                }
            } else {
                NSLog(@"MATCHING PFHUNT NOT FOUND");
            }
        }
    }
    
    [self.tableView reloadData];
}

- (void)runQuery {
    //go grab the same hunts from the cloud (to find the winner count)
    
    NSMutableArray *huntIDs = [self createdHunts];
    PFQuery *query = [PFQuery queryWithClassName:@"Hunt"];
    [query whereKey:@"huntID" containedIn:huntIDs];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            // The find succeeded.
            pfHunts = [[NSMutableArray alloc] initWithArray:objects];
            
            [self populateWinnerCount];
        } else {
            // Log details of the failure
            NSLog(@"Error: %@ %@", error, [error userInfo]);
        }
    }];
}

- (NSMutableArray *)createdHunts {
    NSMutableArray *huntIDs = [[NSMutableArray alloc] init];
    for (int i=0; i < hunts.count ;i++){
        NSString *huntID = [[hunts objectAtIndex:i] valueForKey:@"huntID"];
        [huntIDs addObject:huntID];
    }
    return huntIDs;
}



- (void)goHome:(id)sender
{
    self.title = @"";
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void) viewWillAppear:(BOOL)animated {
    [locationManager startUpdatingLocation];
}

- (void) viewWillDisappear:(BOOL)animated {
    [locationManager stopUpdatingLocation];
    if ([self.navigationController.viewControllers indexOfObject:self] == NSNotFound)
    {
        [self.navigationController popToRootViewControllerAnimated:YES];
        // your view controller already out of the stack, it means user pressed Back button
    }
}

- (void)viewDidUnload
{
    [self setLocationManager:nil];
    [super viewDidUnload];
    self.fetchedResultsController = nil;
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (NSFetchedResultsController *)fetchedResultsController {
    
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Hunt" inManagedObjectContext:managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSSortDescriptor *sort = [[NSSortDescriptor alloc] initWithKey:@"dateCreated" ascending:NO];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObject:sort]];
    
    [fetchRequest setFetchBatchSize:100];
    
    NSFetchedResultsController *theFetchedResultsController =
    [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                        managedObjectContext:managedObjectContext sectionNameKeyPath:nil
                                                   cacheName:@"Root"];
    self.fetchedResultsController = theFetchedResultsController;
    _fetchedResultsController.delegate = self;
    
    return _fetchedResultsController;
    
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id  sectionInfo = [[_fetchedResultsController sections] objectAtIndex:section];
    return [sectionInfo numberOfObjects];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100;
}

- (void) setEditing:(BOOL)editing animated:(BOOL)animated {
    [super setEditing:editing animated:animated];
    [self.tableView setEditing:editing animated:animated];
}


- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    Hunt *thisHunt = [_fetchedResultsController objectAtIndexPath:indexPath];

    NSString *clueText = [thisHunt valueForKey:@"clue"];
    cell.contentView.autoresizesSubviews = YES;
    cell.textLabel.text = [NSString stringWithFormat:@"%@ ",clueText];
    UIImage *image = [UIImage imageWithData:[thisHunt valueForKey:@"thumb"]];
    cell.imageView.image = image;
    
    float totalAccuracy = currentLocation.horizontalAccuracy;
    if([thisHunt valueForKey:@"locAccuracy"]) {
        totalAccuracy = totalAccuracy + [[thisHunt valueForKey:@"locAccuracy"] floatValue];
    }
    
    NSString *detailText = [Helper prettyDistance:[[thisHunt valueForKey:@"distance"] floatValue] withAccuracy:totalAccuracy];
    cell.detailTextLabel.numberOfLines = 2;
    cell.detailTextLabel.lineBreakMode = UILineBreakModeWordWrap;
    
    if (thisHunt.winnerCount && ([thisHunt.winnerCount intValue] > 0)) {
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ \n ", detailText];
        if(![cell viewWithTag:999]) {
            //add the winner label
            UILabel *winnerLabel = [[UILabel alloc] init];
            winnerLabel.tag = 999;
            float winnerHeight = 18;
            float winnerX = 110;
            float winnerTop = 62;
            float winnerWidth = cell.contentView.frame.size.width - cell.imageView.frame.size.width - 10;
            
            winnerLabel.frame = CGRectMake(winnerX, winnerTop, winnerWidth, winnerHeight);
            NSLog(@"adding label. winnerX = %f", winnerX);
            winnerLabel.autoresizingMask =  UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth;
            winnerLabel.font = cell.detailTextLabel.font;
            winnerLabel.textColor = [UIColor orangeColor];
            winnerLabel.text = [NSString stringWithFormat:@"winners: %d", [thisHunt.winnerCount intValue]];
            [cell.contentView addSubview:winnerLabel];
        }
    } else {
        cell.detailTextLabel.text = detailText;
        if([cell viewWithTag:999]) {
            [[cell.contentView viewWithTag:999] removeFromSuperview];
        }
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:@"huntSegue" sender:self];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    // Set up the cell...
    [self configureCell:cell atIndexPath:indexPath];

    return cell;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete NSManagedObject
        NSManagedObject *object = [self.fetchedResultsController objectAtIndexPath:indexPath];
        NSString *huntID = [object valueForKey:@"huntID"];
        int winnerCount = [[object valueForKey:@"winnerCount"] intValue];
        [managedObjectContext deleteObject:object];
        
        // Save
        NSError *error;
        if ([managedObjectContext save:&error] == NO) {
            // Handle Error.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        } else {
            //if context is saved, remove the object from Parse
            PFQuery *query = [PFQuery queryWithClassName:@"Hunt"];
            [query whereKey:@"huntID" equalTo:huntID];
            [query getFirstObjectInBackgroundWithBlock:^(PFObject *pfobject, NSError *error) {
                if (error) {
                    NSLog(@"Error: %@ %@", error, [error userInfo]);
                } else if (!pfobject) {
                    NSLog(@"Unable to find the hunt that was just deleted.");
                } else {
                    // The object was found
                    [pfobject deleteInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                        if (succeeded) {
                            //DO THIS EVENTUALLY mark the winners (if any) as orphaned
                            if (winnerCount > 0) {
                                NSLog(@"This should be updating the winners to mark them orphans");
                            }
                        } else {
                            NSLog(@"Attempt to delete the hunt failed. Error: %@ %@", error, [error userInfo]);
                        }
                    }];
                }
            }];
            
        }
    }
}

- (void)tableView:(UITableView *)aTableView willBeginEditingRowAtIndexPath:(NSIndexPath *)indexPath {
    // Tell the cell to do its custom animations
    
    editFromSwipe = YES;
}

-(void)tableView:(UITableView *)tableView didEndEditingRowAtIndexPath:(NSIndexPath *)indexPath
{
    editFromSwipe = NO;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    if([cell viewWithTag:999]) {
        UIView *winnerLabel = [cell viewWithTag:999];
        
        if (!editFromSwipe) {
            //this is a kludge, dealing with edit mode shifting of cell.contentView
            int deltaX = self.editing ? 10 : (winnerLabel.frame.origin.x == 110 ? 0: -10);
            winnerLabel.frame = CGRectMake(winnerLabel.frame.origin.x + deltaX, winnerLabel.frame.origin.y, winnerLabel.frame.size.width, winnerLabel.frame.size.height);
            NSLog(@"canEdit. self.editing: %@ winnerX = %f", self.editing ? @"TRUE" : @"FALSE", winnerLabel.frame.origin.x);
        }
    }
    return YES;
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // The table view should not be re-orderable.
    return NO;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    NSManagedObject *object = [[self fetchedResultsController] objectAtIndexPath:indexPath];
    [[segue destinationViewController] setHunt:object];
    if([segue.identifier isEqualToString: @"winnerSegue"]) {
        [[segue destinationViewController] setUserCreated:YES];
    }
}

#pragma mark - Fetched results controller

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    // The fetch controller is about to start sending change notifications, so prepare the table view for updates.
    [self.tableView beginUpdates];
}


- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {
    
    UITableView *tableView = self.tableView;
    
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray
                                               arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray
                                               arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}


- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id )sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
    
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    // The fetch controller has sent all current change notifications, so tell the table view to process all updates.
    [self.tableView endUpdates];
}


// ---------- CLLocationManager delegate functions ---------------

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    
    
    NSLog(@"new location lat: %f lon: %f", newLocation.coordinate.latitude, newLocation.coordinate.longitude);
    currentLocation = newLocation;
    
    if (currentLocation.horizontalAccuracy < 20) {
        [manager stopUpdatingLocation];
    }
    
    for (int i = 0; i < [[_fetchedResultsController fetchedObjects] count]; i++) {
        Hunt *thisHunt = [[_fetchedResultsController fetchedObjects] objectAtIndex:i];
        CLLocation *destLoc = [[CLLocation alloc] initWithLatitude:[[thisHunt valueForKey:@"lat"] floatValue] longitude:[[thisHunt valueForKey:@"lon"] floatValue]];
        
        float distance = [currentLocation distanceFromLocation:destLoc];
        [thisHunt setValue:[NSNumber numberWithFloat:distance] forKey:@"distance"];
    }
    if (!self.editing) {
        [self.tableView reloadData];
    }
}

@end
