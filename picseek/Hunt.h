//
//  Hunt.h
//  picseek
//
//  Created by Ethan Lowry on 11/8/12.
//  Copyright (c) 2012 Ethan Lowry. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Hunt : NSManagedObject

@property (nonatomic, retain) NSData * blur;
@property (nonatomic, retain) NSString * clue;
@property (nonatomic, retain) NSDate * dateCreated;
@property (nonatomic, retain) NSNumber * distance;
@property (nonatomic, retain) NSString * huntID;
@property (nonatomic, retain) NSNumber * lat;
@property (nonatomic, retain) NSNumber * locAccuracy;
@property (nonatomic, retain) NSNumber * lon;
@property (nonatomic, retain) NSData * photo;
@property (nonatomic, retain) NSData * thumb;
@property (nonatomic, retain) NSString * username;
@property (nonatomic, retain) NSNumber * winnerCount;
@property (nonatomic, retain) NSString * picseekUID;

@end
