//
//  ClueViewController.m
//  picseek
//
//  Created by Ethan Lowry on 9/7/12.
//  Copyright (c) 2012 Ethan Lowry. All rights reserved.
//

#import <Parse/Parse.h>
#import "ClueViewController.h"
#import "Hunt.h"
#import "ShareViewController.h"
#import "StartViewController.h"
#import "Helper.h"

#define MIN_ACCURACY 100
#define GREAT_ACCURACY 5

@interface ClueViewController ()

@end

@implementation ClueViewController

@synthesize managedObjectContext;

@synthesize clueField;
@synthesize lat;
@synthesize lon;
@synthesize locAccuracy;
@synthesize photo;
@synthesize blur;
@synthesize thumb;
@synthesize thumbImage;
@synthesize btnSave;
@synthesize placeholderLabel;
@synthesize locationManager;

Hunt *hunt;
BOOL closeEnough;
BOOL waitingForLocation;
int attempts;

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    
    NSLog(@"new location lat: %f lon: %f accuracy: %f", newLocation.coordinate.latitude, newLocation.coordinate.longitude, newLocation.horizontalAccuracy);
    NSLog(@"LOC ACCURACY: %f", locAccuracy);
    
    attempts++;

    NSDate* eventDate = newLocation.timestamp;
    NSTimeInterval howRecent = abs([eventDate timeIntervalSinceNow]);
    
    if ((newLocation.horizontalAccuracy < oldLocation.horizontalAccuracy || !locAccuracy) && howRecent < 60) {
        lat = newLocation.coordinate.latitude;
        lon = newLocation.coordinate.longitude;
        locAccuracy = newLocation.horizontalAccuracy;
    }
    
    if (locAccuracy <= MIN_ACCURACY || attempts > 5) {
        NSLog(@"LOC SAYS WE'RE CLOSE! (attempts: %d)", attempts);
        closeEnough = YES;
        if (waitingForLocation) {
            NSLog(@"LOC SAYS SAVE!");
            [locationManager stopUpdatingLocation];
            [self saveHunt];
            [self hideInstructions];
        }
    }
    
    if (locAccuracy <= GREAT_ACCURACY) {
        [locationManager stopUpdatingLocation];
    }
    
}

- (IBAction)saveBtnClicked:(id)sender {
    
    if (!closeEnough) {
        NSLog(@"NOT CLOSE ENOUGH");
        waitingForLocation = YES;
        
        [self showInstructions];        
        [btnSave setEnabled:NO];
        [clueField setEditable:NO];
        
        // set up a timer to give up?
    } else {
        NSLog(@"CLOSE ENOUGH");
        [self saveHunt];
    }
}

- (void)saveHunt {
    //SAVE LOCAL COPY
    NSLog(@"SAVING!");
    hunt = [NSEntityDescription insertNewObjectForEntityForName:@"Hunt" inManagedObjectContext:managedObjectContext];
    
    hunt.clue = clueField.text;
    hunt.photo = UIImagePNGRepresentation(photo);
    hunt.blur = UIImagePNGRepresentation(blur);
    hunt.thumb = UIImagePNGRepresentation(thumb);
    hunt.lat = [NSNumber numberWithFloat:lat];
    hunt.lon = [NSNumber numberWithFloat:lon];
    hunt.locAccuracy = [NSNumber numberWithFloat:locAccuracy];
    hunt.huntID = (__bridge NSString *)CFUUIDCreateString(NULL, CFUUIDCreate(NULL));
    hunt.dateCreated = [NSDate date];
    hunt.username = [Helper username];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    hunt.picseekUID = [defaults valueForKey:@"picseekUID"];
    
    //add this to the list of user created hunts
    NSArray *createdHunts = [defaults stringArrayForKey:@"createdHunts"];
    //if the created hunt list doesn't include the current hunt yet, add it
    if (![createdHunts containsObject:[hunt valueForKey:@"huntID"]]) {
        NSMutableArray *newHunts = [[NSMutableArray alloc] initWithArray:createdHunts];
        [newHunts addObject:[hunt valueForKey:@"huntID"]];
        [defaults setObject:newHunts forKey:@"createdHunts"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    //svc.hunt = hunt;
    
    NSError *error;
    if (![managedObjectContext save:&error]) {
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
    }
    else {
        NSLog(@"Hunt ID: %@", hunt.huntID);
        //SAVE TO PARSE
        //Don't bother saving the clean photo
        PFFile *pfBlur = [PFFile fileWithName:@"blur.png" data:hunt.blur];
        [pfBlur saveInBackground];
        
        PFFile *pfThumb = [PFFile fileWithName:@"thumb.png" data:hunt.thumb];
        [pfThumb saveInBackground];
        
        PFObject *pfHunt = [PFObject objectWithClassName:@"Hunt"];
        [pfHunt setObject:hunt.clue forKey:@"clue"];
        [pfHunt setObject:pfBlur forKey:@"blur"];
        [pfHunt setObject:pfThumb forKey:@"thumb"];
        [pfHunt setObject:hunt.lat forKey:@"lat"];
        [pfHunt setObject:hunt.lon forKey:@"lon"];
        [pfHunt setObject:hunt.locAccuracy forKey:@"locAccuracy"];
        [pfHunt setObject:hunt.huntID forKey:@"huntID"];
        [pfHunt setObject:hunt.username forKey:@"username"];
        [pfHunt setObject:hunt.picseekUID forKey:@"picseekUID"];
        
        PFGeoPoint *point = [PFGeoPoint geoPointWithLatitude:[hunt.lat floatValue] longitude:[hunt.lon floatValue]];
        [pfHunt setObject:point forKey:@"location"];
        
        [pfHunt saveInBackground];
        
        NSString *channel = [NSString stringWithFormat:@"hunt%@", hunt.huntID];
        
        [PFPush subscribeToChannelInBackground:channel];
        
    }
    //perform segue
    [self performSegueWithIdentifier:@"saveSegue" sender:self];
}

- (void)showInstructions {
    UILabel *instructionLabel = [self instructionLabel];
    [self.view addSubview:instructionLabel];
    
    [UIView animateWithDuration:0.5 delay:0.0
                        options: (UIViewAnimationOptionCurveEaseInOut & UIViewAnimationOptionBeginFromCurrentState)
                     animations:^{
                         instructionLabel.alpha = 0.7;
                     }
                     completion:^(BOOL complete){}];
}

- (void)hideInstructions {
    [[self.view viewWithTag:100] removeFromSuperview];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSLog(@"HUNT at segue: %@", hunt);
    //[[segue destinationViewController] setHunt:hunt];
    ShareViewController *svc = [segue destinationViewController];
    [svc setHunt:hunt];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    thumbImage.image = thumb;
    [clueField becomeFirstResponder];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textChanged:) name:UITextViewTextDidChangeNotification object:nil];
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"backgroundTile"]];
}

- (void)viewWillAppear:(BOOL)animated
{
    int keyboardHeight = 216;
    int buttonHeight = 95;
    int gap = 1;
    float screenHeight = self.view.frame.size.height;

    [clueField setFrame: CGRectMake(0,0, 320, screenHeight - buttonHeight - keyboardHeight - 2*gap)];
    
    [btnSave setFrame:CGRectMake(0, clueField.frame.size.height + gap, 320, buttonHeight)];
    [btnSave setTitleColor:[UIColor lightGrayColor] forState:UIControlStateDisabled];
    [btnSave setBackgroundImage:[UIImage imageNamed:@"blue"] forState:UIControlStateNormal];
    [btnSave setBackgroundImage:[UIImage imageNamed:@"bluepressed"] forState:UIControlStateHighlighted];
    [btnSave setEnabled:YES];
    
    [clueField setEditable:YES];
    
    
    NSLog(@"LOC ACCURACY: %f", locAccuracy);
    
    if (locAccuracy > GREAT_ACCURACY || locAccuracy == 0) {
        locationManager.delegate = self;
        attempts = 0;
        [locationManager startUpdatingLocation];
    }
    if (locAccuracy > MIN_ACCURACY || locAccuracy == 0) {
        waitingForLocation = NO;
        closeEnough = NO;
    } else {
        closeEnough = YES;
    }
}

-(void)viewWillDisappear:(BOOL)animated
{
    [locationManager stopUpdatingLocation];
}

- (void)viewDidUnload
{
    [self setClueField:nil];
    [self setThumbImage:nil];
    [self setBtnSave:nil];
    [self setPlaceholderLabel:nil];
    [self setLocationManager:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.

}

- (void) textChanged:(NSNotification *) notification
{
    if (clueField.text) {
        placeholderLabel.hidden = YES;
    } else {
        placeholderLabel.hidden = NO;
    }
}

-(UILabel *)instructionLabel {
    CGRect viewRect = { 20.0, (clueField.frame.size.height - 60)/2, 280.0, 60.0 };
    UILabel *instructionLabel = [[UILabel alloc] initWithFrame:viewRect];
    [instructionLabel setBackgroundColor:[UIColor blackColor]];
    [instructionLabel setTextColor:[UIColor whiteColor]];
    [instructionLabel setAlpha:0.0];
    [instructionLabel setNumberOfLines: 1];
    [instructionLabel setTextAlignment:UITextAlignmentCenter];
    [instructionLabel setFont:[UIFont boldSystemFontOfSize:18]];
    [instructionLabel setTag:100];
    [instructionLabel setText:@"Finding your location..."];
    
    return instructionLabel;
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


@end
