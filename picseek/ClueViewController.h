//
//  ClueViewController.h
//  picseek
//
//  Created by Ethan Lowry on 9/7/12.
//  Copyright (c) 2012 Ethan Lowry. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import <CoreLocation/CoreLocation.h>
#import "Hunt.h"

@interface ClueViewController : UIViewController<CLLocationManagerDelegate>

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (weak, nonatomic) IBOutlet UITextView *clueField;

@property (strong, nonatomic) IBOutlet CLLocationManager *locationManager;
@property (nonatomic) float lat;
@property (nonatomic) float lon;
@property (nonatomic) float locAccuracy;
@property (strong, nonatomic) UIImage *photo;
@property (strong, nonatomic) UIImage *blur;
@property (weak, nonatomic) IBOutlet UIImageView *thumbImage;
@property (strong, nonatomic) UIImage *thumb;
@property (weak, nonatomic) IBOutlet UIButton *btnSave;
@property (weak, nonatomic) IBOutlet UILabel *placeholderLabel;

- (IBAction)saveBtnClicked:(id)sender;

@end
