//
//  ShareViewController.m
//  picseek
//
//  Created by Ethan Lowry on 10/15/12.
//  Copyright (c) 2012 Ethan Lowry. All rights reserved.
//

#import "ShareViewController.h"

@interface ShareViewController ()

@end

@implementation ShareViewController

@synthesize hunt;
@synthesize btnDone;
@synthesize btnInvite;
@synthesize actionLabel;

- (IBAction)openMail:(id)sender
{
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
        mailer.mailComposeDelegate = self;
        [mailer setSubject:@"Can you find this?"];
        NSData *imageData = [hunt valueForKey:@"thumb"];
        [mailer addAttachmentData:imageData mimeType:@"image/png" fileName:@"huntimage"];
        NSString *emailBody =[NSString stringWithFormat:@"I've set up a hunt on picseek. Can you find where I took this picture?\n\n picseek://hunt/%@\n\nDon't have picseek yet? Get the free app here: https://itunes.apple.com/us/app/picseek/id591522053", [hunt valueForKey:@"huntID"]];
        [mailer setMessageBody:emailBody isHTML:NO];
        [self presentModalViewController:mailer animated:YES];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failure"
                                                        message:@"Your device doesn't support the composer sheet"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles: nil];
        [alert show];
    }
}

- (void)goHome:(id)sender
{
    self.title = @"";
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled: you cancelled the operation and no email message was queued.");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved: you saved the email message in the drafts folder.");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail send: the email message is queued in the outbox. It is ready to send.");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail failed: the email message was not saved or queued, possibly due to an error.");
            break;
        default:
            NSLog(@"Mail not sent.");
            break;
    }
    // Remove the mail view
    [self dismissModalViewControllerAnimated:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.navigationItem.hidesBackButton = YES;
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"backgroundTile"]];
}

- (void)viewWillAppear:(BOOL)animated
{
    int buttonHeight = 150;
    float screenHeight = self.view.frame.size.height;
    
    [actionLabel setFrame:CGRectMake(0, 0, 320, (screenHeight - 2*buttonHeight))];
    
    [btnInvite setFrame:CGRectMake(0, actionLabel.frame.size.height, 320, buttonHeight)];
    [btnInvite setBackgroundImage:[UIImage imageNamed:@"orange"] forState:UIControlStateNormal];
    [btnInvite setBackgroundImage:[UIImage imageNamed:@"orangepressed"] forState:UIControlStateHighlighted];
    
    [btnDone setFrame:CGRectMake(0, (actionLabel.frame.size.height + buttonHeight), 320, buttonHeight)];
    [btnDone setBackgroundImage:[UIImage imageNamed:@"blue"] forState:UIControlStateNormal];
    [btnDone setBackgroundImage:[UIImage imageNamed:@"bluepressed"] forState:UIControlStateHighlighted];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setBtnInvite:nil];
    [self setBtnDone:nil];
    [self setActionLabel:nil];
    [super viewDidUnload];
}
@end
