//
//  MessageViewController.m
//  picseek
//
//  Created by Ethan Lowry on 10/27/12.
//  Copyright (c) 2012 Ethan Lowry. All rights reserved.
//

#import "MessageViewController.h"
#import "UIImage+Resize.h"
#import "SolvedViewController.h"
#import "Helper.h"

@interface MessageViewController ()

@end

@implementation MessageViewController

@synthesize solutionImage;
@synthesize elapsedTime;
@synthesize btnSave;
@synthesize placeholderLabel;
@synthesize messageField;
@synthesize hunt;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [messageField becomeFirstResponder];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textChanged:) name:UITextViewTextDidChangeNotification object:nil];
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"backgroundTile"]];
}


- (void)viewWillAppear:(BOOL)animated
{
    int keyboardHeight = 216;
    int buttonHeight = 95;
    int gap = 1;
    float screenHeight = self.view.frame.size.height;
    
    [messageField setFrame: CGRectMake(0,0, 320, screenHeight - buttonHeight - keyboardHeight - 2*gap)];
    
    [btnSave setFrame:CGRectMake(0, messageField.frame.size.height + gap, 320, buttonHeight)];
    [btnSave setBackgroundImage:[UIImage imageNamed:@"blue"] forState:UIControlStateNormal];
    [btnSave setBackgroundImage:[UIImage imageNamed:@"bluepressed"] forState:UIControlStateHighlighted];
    
}

- (void) textChanged:(NSNotification *) notification
{
    if (messageField.text) {
        placeholderLabel.hidden = YES;
    } else {
        placeholderLabel.hidden = NO;
    }
}

- (IBAction)saveBtnClicked:(id)sender {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    //save time
    NSString *keyString = [NSString stringWithFormat:@"%@-solved", [hunt valueForKey:@"huntID"]];
    [defaults setObject:elapsedTime forKey:keyString];
        
    //save photo
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString  *path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png", keyString]];
    NSData* data = UIImagePNGRepresentation(solutionImage);
    [data writeToFile:path atomically:YES];
    
    //save message
    keyString = [NSString stringWithFormat:@"%@-message", [hunt valueForKey:@"huntID"]];
    [defaults setObject:messageField.text forKey:keyString];
    
    //check and remove any old solution by this device
    NSString *picseekUID = [defaults valueForKey:@"picseekUID"];
    
    //force save to user defaults
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSString *wasrecorded = [defaults objectForKey:[NSString stringWithFormat:@"%@-solved", [hunt valueForKey:@"huntID"]]] ? @"YES" : @"NO";
    NSLog(@"WAS RECORDED? %@ : %@", wasrecorded, [NSString stringWithFormat:@"%@-solved", [hunt valueForKey:@"huntID"]]);
    
    
    PFQuery *query = [PFQuery queryWithClassName:@"Winner"];
    [query whereKey:@"picseekUID" equalTo:picseekUID];
    [query whereKey:@"huntID" equalTo:[hunt valueForKey:@"huntID"]];
    PFObject *pfOldWinner = [query getFirstObject];
    if (pfOldWinner) {
        [pfOldWinner deleteInBackground];
        NSLog(@"YOU HAVE AN OLD WINNER (HVC): %@", [pfOldWinner objectForKey:@"huntID"]);
    }
    
    //SAVE TO PARSE
    PFFile *pfPhoto = [PFFile fileWithName:@"photo.png" data:data];
    [pfPhoto saveInBackground];
    PFObject *pfWinner = [PFObject objectWithClassName:@"Winner"];
    [pfWinner setObject:elapsedTime forKey:@"time"];
    [pfWinner setObject:[hunt valueForKey:@"huntID"] forKey:@"huntID"];
    [pfWinner setObject:picseekUID forKey:@"picseekUID"];
    [pfWinner setObject:messageField.text forKey:@"message"];
    [pfWinner setObject:pfPhoto forKey:@"photo"];
    [pfWinner setObject:[Helper username] forKey:@"username"];
    UIImage *thumb = [solutionImage resizedImage:CGSizeMake(100, 100) interpolationQuality:0];
    [pfWinner setObject:UIImagePNGRepresentation(thumb) forKey:@"thumb"];
    [pfWinner saveInBackground];
    
    //now increment the winner count of this hunt
    query = [PFQuery queryWithClassName:@"Hunt"];
    [query whereKey:@"huntID" equalTo:[hunt valueForKey:@"huntID"]];
    PFObject *pfHunt = [query getFirstObject];
    int winnerCount = [[pfHunt valueForKey:@"winnerCount"] intValue];
    if(winnerCount && (winnerCount != 0)) {
        winnerCount = winnerCount + 1;
    } else {
        winnerCount = 1;
    }
    [pfHunt setValue:[NSNumber numberWithInt:winnerCount] forKey:@"winnerCount"];
    [pfHunt saveInBackground];
    
    //now alert the creator of this hunt that it was solved
    if (![picseekUID isEqualToString:[hunt valueForKey:@"picseekUID"]]) {
        NSString *clueForPush = [hunt valueForKey:@"clue"];
        if (clueForPush.length > 30) {
            clueForPush = [[hunt valueForKey:@"clue"] substringWithRange:NSMakeRange(0, 30)];
            clueForPush = [NSString stringWithFormat:@"%@...", clueForPush];
        }
        
        NSString *pushString = [NSString stringWithFormat:@"%@ solved your hunt: \"%@\"", [Helper username], clueForPush];
        
        NSString *channel = [NSString stringWithFormat:@"hunt%@", [hunt valueForKey:@"huntID"]];
        
        [PFPush sendPushMessageToChannelInBackground:channel
                                         withMessage:pushString];
    } else {
        NSLog(@"NO NOTIFICATION. IT'S YOUR OWN HUNT");
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    [[segue destinationViewController] setHunt:hunt];
    [[segue destinationViewController] setSolutionImage:solutionImage];
    [[segue destinationViewController] setElapsedTime:elapsedTime];
    [[segue destinationViewController] setShowHomeButton:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setBtnSave:nil];
    [self setPlaceholderLabel:nil];
    [self setMessageField:nil];
    [super viewDidUnload];
}

@end
