//
//  StartViewController.h
//  picseek
//
//  Created by Ethan Lowry on 9/7/12.
//  Copyright (c) 2012 Ethan Lowry. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StartViewController : UIViewController <NSFetchedResultsControllerDelegate, UITextFieldDelegate>

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;

@property (weak, nonatomic) IBOutlet UIButton *btnCreate;
@property (weak, nonatomic) IBOutlet UIButton *btnCurrent;
@property (weak, nonatomic) IBOutlet UIButton *btnNearby;
@property (weak, nonatomic) IBOutlet UIButton *btnCreated;
@property (weak, nonatomic) IBOutlet UIButton *btnCompleted;

@property (weak, nonatomic) IBOutlet UIView *nameView;
@property (weak, nonatomic) IBOutlet UIView *firstTimeView;
@property (weak, nonatomic) IBOutlet UITextField *nameField;
@property (weak, nonatomic) IBOutlet UIButton *btnSave;
@property (weak, nonatomic) IBOutlet UILabel *nameInstructionLabel;

- (IBAction)saveBtnClicked:(id)sender;


@end
