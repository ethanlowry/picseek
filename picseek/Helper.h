//
//  Helper.h
//  picseek
//
//  Created by Ethan Lowry on 9/25/12.
//  Copyright (c) 2012 Ethan Lowry. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Helper : NSObject
+ (NSString *)prettyDistance:(float)distance withAccuracy:(float)accuracy;
+ (NSString *)prettyTime:(int)seconds;
+ (NSString *)username;
@end
