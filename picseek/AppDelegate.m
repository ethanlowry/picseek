//
//  AppDelegate.m
//  picseek
//
//  Created by Ethan Lowry on 9/7/12.
//  Copyright (c) 2012 Ethan Lowry. All rights reserved.
//

#import "AppDelegate.h"

#import "StartViewController.h"
#import "Parse/Parse.h"
#import "Hunt.h"
#import "HuntViewController.h"
#import "StartViewController.h"
#import "Helper.h"


@implementation AppDelegate


@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

UINavigationController *navigationController;



//URL scheme handler
- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url
{
    if (!url) {  return NO; }
    
    NSLog(@"HOST: %@, PATH: %@",[url host], [url path]);
    
    if ([url path] && [url host] && [[url host] isEqualToString:@"hunt"])
         {
             NSLog(@"HUNT-PATH:%@", [[url path] substringFromIndex:1]);
             PFQuery *query = [PFQuery queryWithClassName:@"Hunt"];
             [query whereKey:@"huntID" equalTo:[[url path] substringFromIndex:1]];
             
             [query getFirstObjectInBackgroundWithBlock:^(PFObject *pfHunt, NSError *error) {
                 NSLog(@"PFHUNT CLUE: %@", [pfHunt valueForKey:@"clue"]);
                 
                 
                 Hunt *hunt = [NSEntityDescription insertNewObjectForEntityForName:@"Hunt" inManagedObjectContext:_managedObjectContext];

                 [hunt setValue:[pfHunt valueForKey:@"clue"] forKey:@"clue"];
                 [hunt setValue:[pfHunt valueForKey:@"lat"] forKey:@"lat"];
                 [hunt setValue:[pfHunt valueForKey:@"lon"] forKey:@"lon"];
                 [hunt setValue:[pfHunt valueForKey:@"locAccuracy"] forKey:@"locAccuracy"];
                 [hunt setValue:[pfHunt valueForKey:@"huntID"] forKey:@"huntID"];
                 [hunt setValue:[pfHunt valueForKey:@"picseekUID"] forKey:@"picseekUID"];
                 [hunt setValue:[pfHunt valueForKey:@"username"] forKey:@"username"];
                 
                 PFFile *blurFile = [pfHunt objectForKey:@"blur"];
                 NSData *blurData = [blurFile getData];
                 [hunt setValue:blurData forKey:@"blur"];
                 [hunt setValue:blurData forKey:@"photo"];
                 
                 PFFile *thumbFile = [pfHunt objectForKey:@"thumb"];
                 NSData *thumbData = [thumbFile getData];
                 [hunt setValue:thumbData forKey:@"thumb"];
                 
                 NSLog(@"HUNT CLUE: %@", [hunt valueForKey:@"clue"]);
                              
                 HuntViewController *hvc = [self.window.rootViewController.storyboard instantiateViewControllerWithIdentifier:@"huntview"];
                 
                 [hvc setHunt:hunt];
                 [hvc setShowHomeButton:YES];
                 
                 [navigationController pushViewController:hvc animated:YES];
             }];
         }

    return YES;
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    [PFPush storeDeviceToken:deviceToken];
    [PFPush subscribeToChannelInBackground:@""];
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog(@"Failed to register for remote notifications: %@", error);
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    [PFPush handlePush:userInfo];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    // set the user's UID if necessary
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if (![defaults valueForKey:@"picseekUID"]) {
        NSString *picseekUID = (__bridge NSString *)CFUUIDCreateString(NULL, CFUUIDCreate(NULL));
        [defaults setObject:picseekUID forKey:@"picseekUID"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    //NSLog(@"UID: %@", [defaults valueForKey:@"picseekUID"]);
    
    // set up navigation and managed object context
    navigationController = (UINavigationController *)self.window.rootViewController;
    StartViewController *controller = (StartViewController *)navigationController.topViewController;
    controller.managedObjectContext = self.managedObjectContext;
    
    //PARSE set up
    
    // LIVE APP
    [Parse setApplicationId:@"D4yOLeuETS1p7HrE7GwlIvkAUWLDFjhViE70bdT7"
                  clientKey:@"EAKXAV0mvhyeE07RUugfnhj1j8EFargSPGgp6lv9"];
    
    //TEST APP
    //[Parse setApplicationId:@"JChXZQFGHvXMmRA091gNL18aOffakucXV9uJ0axW"
    //              clientKey:@"Vudt40QLFM5HnSMOaVUbj6WRF4zr0E3cJiIw0544"];
    
    [PFUser enableAutomaticUser];
    PFACL *defaultACL = [PFACL ACL];
    // Optionally enable public read access by default. (yeah, hunts are totally public data here)
    [defaultACL setPublicReadAccess:YES];
    
    
    // SET UP PUSH
    [application registerForRemoteNotificationTypes:UIRemoteNotificationTypeAlert|UIRemoteNotificationTypeBadge|UIRemoteNotificationTypeSound];
        
    // set the status bar to black
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleBlackOpaque];
    
    //launch URL scheme handler
    NSURL *urlToParse = [launchOptions objectForKey:UIApplicationLaunchOptionsURLKey];
    if (urlToParse) {
        [self application:application handleOpenURL:urlToParse];
    }
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    [[NSNotificationCenter defaultCenter] postNotificationName:@"background" object:self];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    [[NSNotificationCenter defaultCenter] postNotificationName:@"foreground" object:self];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}

- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
             // Replace this implementation with code to handle the error appropriately.
             // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        } 
    }
}

#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"picseek" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"picseek.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
         @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }    
    
    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

@end
