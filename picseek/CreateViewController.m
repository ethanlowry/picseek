//
//  CreateViewController.m
//  picseek
//
//  Created by Ethan Lowry on 9/7/12.
//  Copyright (c) 2012 Ethan Lowry. All rights reserved.
//

#import "CreateViewController.h"
#import "ClueViewController.h"

@interface CreateViewController ()

@end

@implementation CreateViewController

@synthesize locationManager;
@synthesize instructionsView;
@synthesize managedObjectContext;
@synthesize selectedPhoto;
@synthesize blurredPhoto;
@synthesize cropShape;
@synthesize imagePreview;
@synthesize stillImageOutput;
@synthesize btnRetake;
@synthesize btnNext;
@synthesize btnCamera;
@synthesize controlAreaView;
@synthesize displayAreaView;

float startX = -1;
float startY = -1;

float photoLat;
float photoLon;
float locAccuracy;

BOOL instructionsShown;

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    if (touch.phase == UITouchPhaseBegan) {
        startX = [touch locationInView:selectedPhoto].x;
        startY = [touch locationInView:selectedPhoto].y;
    }
    instructionsView.hidden = YES;
}

- (IBAction)handleTap:(UITapGestureRecognizer *)recognizer {
    cropShape.hidden = YES;
    blurredPhoto.hidden = YES;
    instructionsView.hidden = YES;
}

- (IBAction)handlePan:(UIPanGestureRecognizer *)recognizer {
    recognizer.cancelsTouchesInView = NO;
    blurredPhoto.hidden = YES;
    CGPoint translation = [recognizer translationInView:self.view];
    
    if (selectedPhoto.hidden == NO) {
        if (recognizer.state == UIGestureRecognizerStateBegan) {
            cropShape.hidden = NO;
        } else if (recognizer.state == UIGestureRecognizerStateEnded) {
            [self executeBlur:recognizer];
        }
    }
    
    cropShape.frame = CGRectMake (((translation.x < 0) ? startX + translation.x : startX), ((translation.y < 0) ? startY + translation.y : startY), fabsf(translation.x), fabsf(translation.y));
}

- (IBAction)handleCropPan:(UIPanGestureRecognizer *)recognizer {
    CGPoint translation = [recognizer translationInView:self.view];
    recognizer.view.center = CGPointMake(recognizer.view.center.x + translation.x,
                                         recognizer.view.center.y + translation.y);
    [recognizer setTranslation:CGPointMake(0, 0) inView:self.view];
}

- (IBAction)photoBtnClicked:(id)sender {
    AVCaptureConnection *videoConnection = nil;
	for (AVCaptureConnection *connection in stillImageOutput.connections)
	{
		for (AVCaptureInputPort *port in [connection inputPorts])
		{
			if ([[port mediaType] isEqual:AVMediaTypeVideo] )
			{
				videoConnection = connection;
				break;
			}
		}
		if (videoConnection) { break; }
	}
    
	NSLog(@"about to request a capture from: %@", stillImageOutput);
	[stillImageOutput captureStillImageAsynchronouslyFromConnection:videoConnection completionHandler: ^(CMSampleBufferRef imageSampleBuffer, NSError *error)
     {
		 CFDictionaryRef exifAttachments = CMGetAttachment( imageSampleBuffer, kCGImagePropertyExifDictionary, NULL);
		 if (exifAttachments)
		 {
             // Do something with the attachments.
             //NSLog(@"attachements: %@", exifAttachments);
		 }
         else
             NSLog(@"no attachments");
         
         NSData *imageData = [AVCaptureStillImageOutput jpegStillImageNSDataRepresentation:imageSampleBuffer];
         UIImage *image = [[UIImage alloc] initWithData:imageData];
         
         //resize and crop the image
         
         UIImage *inputImage = [image resizedImage:CGSizeMake(image.size.width, image.size.height) interpolationQuality:0];
         
         float edge = (inputImage.size.width > inputImage.size.height) ? inputImage.size.height : inputImage.size.width;
         
         CGRect cropRect = CGRectMake(0, 5, edge, edge); //crop origin is set at 0,5 to make it work. not sure why.
         
         UIImage *outputImage = [inputImage croppedImage:cropRect];
         
         //present the selected image
         selectedPhoto.contentMode = UIViewContentModeScaleAspectFit;
         selectedPhoto.image = outputImage;
         blurredPhoto.hidden = YES;
         
         selectedPhoto.hidden = NO;
         if (!instructionsShown) {
             instructionsShown = YES;
             instructionsView.hidden = NO;
         }
         btnNext.hidden = NO;
         btnRetake.hidden = NO;
         btnCamera.hidden = YES;
         imagePreview.hidden = YES;
         [btnRetake setFrame:CGRectMake(0, 1, 213, 95)];
	 }];
}

- (void)executeBlur:(id)sender {
    if (!cropShape.hidden){
        UIImage *tempImage = [[selectedPhoto.image resizedImage:CGSizeMake(320, 320) interpolationQuality:0] stackBlur:30];
        
        float cropWidth = cropShape.frame.size.width;
        float cropHeight = cropShape.frame.size.height;
        float cropX = cropShape.frame.origin.x;
        float cropY = tempImage.size.height - (cropShape.frame.origin.y) - cropHeight;
        
        CIImage *inputImage = [[CIImage alloc] initWithImage:tempImage];
        CIContext *context = [CIContext contextWithOptions:nil];
        CIFilter *filter= [CIFilter filterWithName:@"CICrop"];
        [filter setValue:inputImage forKey:@"inputImage"];
        
        [filter setValue:[CIVector vectorWithCGRect:CGRectMake(cropX, cropY, cropWidth, cropHeight)] forKey:@"inputRectangle"];
        
        UIImage *outputImage = [UIImage imageWithCGImage:[context createCGImage:filter.outputImage fromRect:filter.outputImage.extent]];
        
        blurredPhoto.frame = cropShape.frame;
        blurredPhoto.image = outputImage;
        cropShape.hidden = YES;
        blurredPhoto.hidden = NO;
    }
}

- (IBAction)retakeBtnClicked:(id)sender {
    selectedPhoto.hidden = YES;
    blurredPhoto.hidden = YES;
    cropShape.hidden = YES;
    btnRetake.hidden = YES;
    btnCamera.hidden = NO;
    btnNext.hidden = YES;
    imagePreview.hidden = NO;
    [btnRetake setFrame:CGRectMake(0, 1, 213, 95)];
}



// ------ image picker delegate functions ------

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)selectedImage editingInfo:(NSDictionary *)editingInfo {
    
    UIImage *inputImage = [selectedImage resizedImage:CGSizeMake(selectedImage.size.width, selectedImage.size.height) interpolationQuality:0];
    
    float edge = (inputImage.size.width > inputImage.size.height) ? inputImage.size.height : inputImage.size.width;
    float cropX = (inputImage.size.width - edge)/2;
    float cropY = (inputImage.size.height - edge)/2;
    
    CGRect cropRect = CGRectMake(cropX, cropY, edge, edge);

    UIImage *outputImage = [inputImage croppedImage:cropRect];
    
    //present the selected image
    selectedPhoto.contentMode = UIViewContentModeScaleAspectFit;
    selectedPhoto.image = outputImage;
    blurredPhoto.hidden = YES;
    [self dismissModalViewControllerAnimated:YES];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
	[self dismissModalViewControllerAnimated:YES];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    
    NSLog(@"new location lat: %f lon: %f accuracy: %f", newLocation.coordinate.latitude, newLocation.coordinate.longitude, newLocation.horizontalAccuracy);

    NSDate* eventDate = newLocation.timestamp;
    NSTimeInterval howRecent = abs([eventDate timeIntervalSinceNow]);
    NSLog(@"HOW RECENT: %f", howRecent);
    
    if ((newLocation.horizontalAccuracy < oldLocation.horizontalAccuracy || !locAccuracy) && howRecent < 60) {
        photoLat = newLocation.coordinate.latitude;
        photoLon = newLocation.coordinate.longitude;
        locAccuracy = newLocation.horizontalAccuracy;
        NSLog(@"loc accuracy kept: %f", locAccuracy);
    }
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    ClueViewController *controller = [segue destinationViewController];
    controller.managedObjectContext = self.managedObjectContext;
    controller.photo = selectedPhoto.image;
    controller.lat = photoLat;
    controller.lon = photoLon;
    controller.locAccuracy = locAccuracy;

    CGContextRef cgContext;
    
    //create a blurred image
    if (!blurredPhoto.hidden) {
        UIGraphicsBeginImageContext(CGSizeMake(320, 320));
        cgContext = UIGraphicsGetCurrentContext();
        
        [[selectedPhoto.image resizedImage:CGSizeMake(320, 320) interpolationQuality:0] drawInRect:CGRectMake(0, 0, 320, 320)];
        [blurredPhoto.image drawInRect:CGRectMake(blurredPhoto.frame.origin.x, blurredPhoto.frame.origin.y, blurredPhoto.frame.size.width, blurredPhoto.frame.size.height)];
        
        controller.blur = [UIImage imageWithCGImage:CGBitmapContextCreateImage(cgContext)];
        UIGraphicsEndImageContext();
        
        //create a thumbnail
        controller.thumb = [controller.blur resizedImage:CGSizeMake(100, 100) interpolationQuality:0];
        
    } else {
        
        controller.blur = controller.photo;
    }
    
    //create a thumbnail
    controller.thumb = [controller.blur resizedImage:CGSizeMake(100, 100) interpolationQuality:0];
}

-(void) viewDidAppear:(BOOL)animated
{
	AVCaptureSession *session = [[AVCaptureSession alloc] init];
	session.sessionPreset = AVCaptureSessionPresetMedium;
    
	AVCaptureVideoPreviewLayer *captureVideoPreviewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:session];
    
	captureVideoPreviewLayer.frame = self.imagePreview.bounds;
	[self.imagePreview.layer addSublayer:captureVideoPreviewLayer];
    
	AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    
	NSError *error = nil;
	AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:device error:&error];
	if (!input) {
		// Handle the error appropriately.
		NSLog(@"ERROR: trying to open camera: %@", error);
	} else {
        [session addInput:input];
        
        stillImageOutput = [[AVCaptureStillImageOutput alloc] init];
        NSDictionary *outputSettings = [[NSDictionary alloc] initWithObjectsAndKeys: AVVideoCodecJPEG, AVVideoCodecKey, nil];
        [stillImageOutput setOutputSettings:outputSettings];
        
        [session addOutput:stillImageOutput];
        
        [session startRunning];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    locationManager.delegate = self;
    [locationManager startUpdatingLocation];
    
    instructionsShown = NO;
    
    int buttonHeight = 95;
    int gap = 1;
    int controlAreaHeight = buttonHeight + gap;
    float screenHeight = self.view.frame.size.height;
    
    [displayAreaView setFrame: CGRectMake(0,(screenHeight - controlAreaHeight - 320)/2, 320, 320)];
    [controlAreaView setFrame: CGRectMake(0, screenHeight - controlAreaHeight, 320, controlAreaHeight)];
    
    [btnCamera setFrame:CGRectMake(0, gap, 320, buttonHeight)];
    [btnCamera setBackgroundImage:[UIImage imageNamed:@"orange"] forState:UIControlStateNormal];
    [btnCamera setBackgroundImage:[UIImage imageNamed:@"orangepressed"] forState:UIControlStateHighlighted];
    [btnCamera setImage:[UIImage imageNamed:@"cameraicon"] forState:UIControlStateNormal];
    
    cropShape.hidden = YES;
    
    [btnRetake setFrame:CGRectMake(0, gap, 213, buttonHeight)];
    [btnRetake setBackgroundImage:[UIImage imageNamed:@"orange"] forState:UIControlStateNormal];
    [btnRetake setBackgroundImage:[UIImage imageNamed:@"orangepressed"] forState:UIControlStateHighlighted];
    
    [btnNext setFrame:CGRectMake(214, gap, 106, buttonHeight)];
    [btnNext setBackgroundImage:[UIImage imageNamed:@"blue"] forState:UIControlStateNormal];
    [btnNext setBackgroundImage:[UIImage imageNamed:@"bluepressed"] forState:UIControlStateHighlighted];

}


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"greennav"] forBarMetrics:UIBarMetricsDefault];
    
    UIBarButtonItem *homeButton = [[UIBarButtonItem alloc] initWithTitle:@"home" style:UIBarButtonItemStyleBordered target:self action:@selector(goHome:)];
    self.navigationItem.leftBarButtonItem = homeButton;
        
    controlAreaView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"backgroundTile"]];
}

- (void)goHome:(id)sender
{
    self.title = @"";
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [locationManager stopUpdatingLocation];
}

- (void)viewDidUnload
{
    [self setSelectedPhoto:nil];
    [self setBlurredPhoto:nil];
    [self setCropShape:nil];
    [self setLocationManager:nil];
    [self setImagePreview:nil];
    [self setBtnRetake:nil];
    [self setInstructionsView:nil];
    [self setBtnNext:nil];
    [self setBtnCamera:nil];
    [self setControlAreaView:nil];
    [self setDisplayAreaView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}




@end
