//
//  HuntViewController.m
//  picseek
//
//  Created by Ethan Lowry on 9/7/12.
//  Copyright (c) 2012 Ethan Lowry. All rights reserved.
//

#import "HuntViewController.h"
#import "Helper.h"
#import "SolvedViewController.h"
#import "ListViewController.h"
#import "AllHuntsListViewController.h"
#import "MessageViewController.h"
#import <Parse/Parse.h>

#define MIN_DISTANCE 20


@interface HuntViewController ()
-(void)timeChanged;
-(void)startTimer;
-(void)stopTimer;
@end

@implementation HuntViewController

@synthesize locationManager;
@synthesize hunt;
@synthesize clueAreaView;
@synthesize clueView;
@synthesize huntPhoto;
@synthesize solutionPhoto;
@synthesize imagePreview;
@synthesize stillImageOutput;
@synthesize distLabel;
@synthesize rightWinnersButton;
@synthesize rightInviteButton;
@synthesize overlayButtons;
@synthesize btnSave;
@synthesize btnRetake;
@synthesize btnCamera;
@synthesize btnShowResults;
@synthesize btnSeeClue;
@synthesize btnSolve;
@synthesize timerLabel;
@synthesize hideControls;
@synthesize compass;
@synthesize compassView;
@synthesize showHomeButton;
@synthesize displayAreaView;
@synthesize btnCancel;

float locHeading = -1;
CLLocation *destLoc;
NSTimeInterval startTime;
NSTimeInterval elapsedTime;
NSTimer *timer;
UIImage *huntImage;


BOOL showTimer;
BOOL showInstructions;
BOOL showOverlayButton;
BOOL animateCompass = YES;
BOOL inZone = NO;
BOOL isSolved;


- (void)viewDidLoad
{
    [super viewDidLoad];
    
	// watch to see every time the view comes in and out of focus, to start and stop the timer appropriately
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveBackgroundNotification:) name:@"background" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveForegroundNotification:) name:@"foreground" object:nil];
    
    [self configureHunt];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSArray *defaultHunts = [defaults stringArrayForKey:@"currentHunts"];
    //if the default hunt list doesn't include the current hunt yet, add it
    if (![defaultHunts containsObject:[hunt valueForKey:@"huntID"]]) {
        NSMutableArray *newHunts = [[NSMutableArray alloc] initWithArray:defaultHunts];
        [newHunts addObject:[hunt valueForKey:@"huntID"]];
        [defaults setObject:newHunts forKey:@"currentHunts"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    NSString *keyString = [NSString stringWithFormat:@"%@-solved", [hunt valueForKey:@"huntID"]];
    if ([defaults stringForKey:keyString]) {
        // the user has already solved this one
        isSolved = YES;
        showInstructions = NO;
        
        //load the image
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png",keyString]];
        
        solutionPhoto.image = [UIImage imageWithContentsOfFile:path];
        
        //load the time
        timerLabel.text = [defaults valueForKey:keyString];
        
        //hide controls
        hideControls.hidden = NO;
        compassView.hidden = YES;
        timerLabel.hidden = YES;
    } else {
        // an unsolved hunt!
        isSolved = NO;
        showInstructions = YES;
        locationManager.delegate = self;
        [locationManager startUpdatingLocation];
        
        if ([CLLocationManager headingAvailable]) {
            locationManager.headingFilter = 5;
            [locationManager startUpdatingHeading];
        }
        
        //reset controls
        clueAreaView.hidden = NO;
        overlayButtons.hidden = YES;
        hideControls.hidden = YES;
        [self startTimer];
        [self.view addSubview:[self instructionLabel]];
    }
    
    // if the hunt is solved, or it is a hunt this user created, present the "show winners" right nav button
    NSArray *createdHunts = [defaults stringArrayForKey:@"createdHunts"];
    BOOL userCreated = [createdHunts containsObject:[hunt valueForKey:@"huntID"]];
    if (isSolved || userCreated) {
        self.navigationItem.rightBarButtonItem = self.rightWinnersButton;
    } else {
        self.navigationItem.rightBarButtonItem = self.rightInviteButton;
    }
}


// LOCATION STUFF
- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
        
    float distance = [newLocation distanceFromLocation:destLoc];
    float totalAccuracy = newLocation.horizontalAccuracy + [[hunt valueForKey:@"locAccuracy"] floatValue];
    
    distLabel.text = [Helper prettyDistance:distance withAccuracy:newLocation.horizontalAccuracy];
    locHeading = getHeading(newLocation.coordinate, destLoc.coordinate);

    inZone = [self inZone:distance withAccuracy:totalAccuracy];
    
    if (inZone) {
        NSLog(@"IN ZONE!");
        // show instructions (unless they've been shown already)
        // show/hide compass
        // show instructions (if flag set, and inZone)
        // set flag to show explanation/vs overlay
        // start/stop updating heading

        if (showInstructions) {
            NSLog(@"SHOW INSTRUCTIONS!");
            NSString *instructionsText = @"YOU ARE CLOSE\nTap the \"solve it\" button\nto take a picture from\nthe same location\nas the original photo";
            [self showInstructions:instructionsText withDuration:0.5];
            showInstructions = NO;
        }
        compassView.hidden = YES;
        [locationManager stopUpdatingHeading];
    } else {
        if ([CLLocationManager headingAvailable]) {
            locationManager.headingFilter = 5;
            [locationManager startUpdatingHeading];
        }
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateHeading:(CLHeading *)newHeading {
    if (newHeading.headingAccuracy < 0)
        return;
    
    // Use the true heading if it is valid.
    CLLocationDirection  currentHeading = ((newHeading.trueHeading > 0) ?
                                           newHeading.trueHeading : newHeading.magneticHeading);
    
    //NSLog(@"current heading: %f", currentHeading);
    
    float delta = locHeading - currentHeading;
    [self showCompass];
    if (animateCompass) {
        animateCompass = NO;
        [UIView animateWithDuration:0.5 delay:0.0
                            options: (UIViewAnimationOptionCurveEaseInOut & UIViewAnimationOptionBeginFromCurrentState)
                         animations:^{
                             compass.transform = CGAffineTransformMakeRotation(M_PI * delta/180);
                         }
                         completion:^(BOOL complete){
                             animateCompass = YES;
                         }];
    }
}

- (void)showCompass {
    if (compassView.hidden) {
        compassView.alpha = 0.0;
        compassView.hidden = NO;
        [UIView animateWithDuration:0.5 delay:0.0
                            options: (UIViewAnimationOptionCurveEaseInOut & UIViewAnimationOptionBeginFromCurrentState)
                         animations:^{
                             compassView.alpha = 1.0;
                         }
                         completion:^(BOOL complete){}];
    }
}

- (IBAction)photoBtnClicked:(id)sender {
    
    AVCaptureConnection *videoConnection = nil;
	for (AVCaptureConnection *connection in stillImageOutput.connections)
	{
		for (AVCaptureInputPort *port in [connection inputPorts])
		{
			if ([[port mediaType] isEqual:AVMediaTypeVideo] )
			{
				videoConnection = connection;
				break;
			}
		}
		if (videoConnection) { break; }
	}
    
	NSLog(@"about to request a capture from: %@", stillImageOutput);
	[stillImageOutput captureStillImageAsynchronouslyFromConnection:videoConnection completionHandler: ^(CMSampleBufferRef imageSampleBuffer, NSError *error)
     {
         NSData *imageData = [AVCaptureStillImageOutput jpegStillImageNSDataRepresentation:imageSampleBuffer];
         UIImage *image = [[UIImage alloc] initWithData:imageData];

         //resize and crop the image
         
         NSLog(@"photo width: %g, height: %g", image.size.width, image.size.height);
         UIImage *inputImage = [image resizedImage:CGSizeMake(image.size.width, image.size.height) interpolationQuality:0];
         
         float edge = (inputImage.size.width > inputImage.size.height) ? inputImage.size.height : inputImage.size.width;
         
         CGRect cropRect = CGRectMake(0, 5, edge, edge); //crop origin is set at 0,5 to make it work. not sure why.
         
         UIImage *outputImage = [inputImage croppedImage:cropRect];
         
         //present the selected image
         huntPhoto.image = outputImage;
         solutionPhoto.image = outputImage;
         huntPhoto.alpha = 1;
         btnSave.hidden = NO;
         btnRetake.hidden = NO;
         btnCamera.hidden = YES;
         btnCancel.hidden = YES;
	 }];
}

- (IBAction)retakeBtnClicked:(id)sender {
    huntPhoto.image = huntImage;
    huntPhoto.alpha = .5;
    btnSave.hidden = YES;
    btnRetake.hidden = YES;
    btnCamera.hidden = NO;
    btnCancel.hidden = NO;
}

- (IBAction)saveBtnClicked:(id)sender {
    [self stopTimer];
}

- (void)showOverlay:(id)sender
{
    if (imagePreview.hidden) {
        if (!inZone) {
            UILabel *instructionLabel = (id)[self.view viewWithTag:100];
            if (instructionLabel.alpha > 0) {
                [self hideInstructions:0];
            } else {
                NSString *explanatoryText = @"NOT QUITE THERE YET\nAccording to your iPhone\nyou are not in the right\nplace just yet. Try again\nwhen you get there!";
                [self showInstructions:explanatoryText withDuration:1.0];
            }
        } else {
            [self hideInstructions:0];
            imagePreview.hidden = NO;
            huntPhoto.alpha = .5;
            overlayButtons.hidden = NO;
        }
    } else {
        imagePreview.hidden = YES;
        huntPhoto.alpha = 1;
        
        overlayButtons.hidden = YES;
        huntPhoto.image = huntImage;
        btnSave.hidden = YES;
        btnRetake.hidden = YES;
        btnCamera.hidden = NO;
        btnCancel.hidden = NO;
    }
}


-(void) viewWillAppear:(BOOL)animated
{
    if (!isSolved) {
        [locationManager startUpdatingLocation];
        NSLog(@"TIMER HIDDEN? %@", timerLabel.hidden ? @"YES" : @"NO");
        [self startTimer];
    }
    
    //Check to see if this hunt was launched externally. If so, make back button a "Home" button.
    if(showHomeButton){
        UIBarButtonItem *homeButton = [[UIBarButtonItem alloc] initWithTitle:@"home" style:UIBarButtonItemStyleBordered target:self action:@selector(goHome:)];
        self.navigationItem.leftBarButtonItem = homeButton;
        [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"orangenav"] forBarMetrics:UIBarMetricsDefault];
    }
    
    //layout
    int buttonHeight = 95;
    int gap = 1;
    int controlAreaHeight = buttonHeight + gap;
    float screenHeight = self.view.frame.size.height;
    
    [displayAreaView setFrame: CGRectMake(0,(screenHeight - controlAreaHeight - 320)/2, 320, 320)];
    [clueAreaView setFrame: CGRectMake(0, screenHeight - controlAreaHeight, 320, controlAreaHeight)];
    
    [btnSeeClue setFrame:CGRectMake(0, gap, 159, buttonHeight)];
    [btnSeeClue setBackgroundImage:[UIImage imageNamed:@"green"] forState:UIControlStateNormal];
    [btnSeeClue setBackgroundImage:[UIImage imageNamed:@"greenpressed"] forState:UIControlStateHighlighted];
    
    [btnSolve setFrame:CGRectMake(160, gap, 160, buttonHeight)];
    [btnSolve setBackgroundImage:[UIImage imageNamed:@"yellow"] forState:UIControlStateNormal];
    [btnSolve setBackgroundImage:[UIImage imageNamed:@"yellowpressed"] forState:UIControlStateHighlighted];
    
    [btnCamera setFrame:CGRectMake(160, gap, 160, buttonHeight)];
    [btnCamera setBackgroundImage:[UIImage imageNamed:@"orange"] forState:UIControlStateNormal];
    [btnCamera setBackgroundImage:[UIImage imageNamed:@"orangepressed"] forState:UIControlStateHighlighted];
    [btnCamera setImage:[UIImage imageNamed:@"cameraicon"] forState:UIControlStateNormal];
    
    [btnCancel setFrame:CGRectMake(0, gap, 159, buttonHeight)];
    [btnCancel setBackgroundImage:[UIImage imageNamed:@"teal"] forState:UIControlStateNormal];
    [btnCancel setBackgroundImage:[UIImage imageNamed:@"tealpressed"] forState:UIControlStateHighlighted];
    
    [btnRetake setFrame:CGRectMake(0, gap, 159, buttonHeight)];
    [btnRetake setBackgroundImage:[UIImage imageNamed:@"orange"] forState:UIControlStateNormal];
    [btnRetake setBackgroundImage:[UIImage imageNamed:@"orangepressed"] forState:UIControlStateHighlighted];
    
    [btnSave setFrame:CGRectMake(160, gap, 160, buttonHeight)];
    [btnSave setBackgroundImage:[UIImage imageNamed:@"blue"] forState:UIControlStateNormal];
    [btnSave setBackgroundImage:[UIImage imageNamed:@"bluepressed"] forState:UIControlStateHighlighted];
    
    [btnShowResults setFrame:CGRectMake(0, gap, 320, buttonHeight)];
    [btnShowResults setBackgroundImage:[UIImage imageNamed:@"blue"] forState:UIControlStateNormal];
    [btnShowResults setBackgroundImage:[UIImage imageNamed:@"bluepressed"] forState:UIControlStateHighlighted];
    
    // background color
    clueAreaView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"backgroundTile"]];
    overlayButtons.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"backgroundTile"]];
    hideControls.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"backgroundTile"]];
    
    // clue view
    if (!clueView) {
        clueView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, screenHeight)];
        [clueView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"backgroundTile"]]];
        [clueView setAlpha:0.0];
        
        UITextView *clueTextView = [[UITextView alloc] initWithFrame:CGRectMake(20, 20, 280, screenHeight - 40 - buttonHeight - gap)];
        [clueTextView setTextColor:[UIColor whiteColor]];
        [clueTextView setShowsVerticalScrollIndicator:YES];
        [clueTextView setBackgroundColor:[UIColor clearColor]];
        [clueTextView setScrollEnabled:YES];
        [clueTextView setFont:[UIFont boldSystemFontOfSize:18]];
        [clueTextView setText:[hunt valueForKey:@"clue"]];
        [clueView addSubview:clueTextView];
        
        UIButton *btnClueDone = [[UIButton alloc] init];
        [btnClueDone setFrame:CGRectMake(0, clueView.frame.size.height - buttonHeight, 320, buttonHeight)];
        [btnClueDone setBackgroundImage:[UIImage imageNamed:@"blue"] forState:UIControlStateNormal];
        [btnClueDone setBackgroundImage:[UIImage imageNamed:@"bluepressed"] forState:UIControlStateHighlighted];
        [btnClueDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        btnClueDone.titleLabel.font = [UIFont boldSystemFontOfSize:28];
        [btnClueDone setTitle:@"done" forState:UIControlStateNormal];
        [btnClueDone addTarget:self action:@selector(hideClue:) forControlEvents:UIControlEventTouchUpInside];
        [clueView addSubview:btnClueDone];

        [self.view addSubview:clueView];
    }
}

-(void) showClue {
    [UIView animateWithDuration:0.5 delay:0.0
                        options: (UIViewAnimationOptionCurveEaseInOut & UIViewAnimationOptionBeginFromCurrentState)
                     animations:^{
                         clueView.alpha = 1.0;
                     }
                     completion:^(BOOL complete){}];
    
}

-(void) hideClue:(id)sender {
    [UIView animateWithDuration:0.5 delay:0.0
                        options: (UIViewAnimationOptionCurveEaseInOut & UIViewAnimationOptionBeginFromCurrentState)
                     animations:^{
                         clueView.alpha = 0.0;
                     }
                     completion:^(BOOL complete){}];
}


-(void) viewDidAppear:(BOOL)animated
{
    //set up the video preview
    AVCaptureSession *session = [[AVCaptureSession alloc] init];
	session.sessionPreset = AVCaptureSessionPresetMedium;
    
	AVCaptureVideoPreviewLayer *captureVideoPreviewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:session];
    
	captureVideoPreviewLayer.frame = self.imagePreview.bounds;
	[self.imagePreview.layer addSublayer:captureVideoPreviewLayer];
    
	AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    
	NSError *error = nil;
	AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:device error:&error];
	if (!input) {
		// Handle the error appropriately.
		NSLog(@"ERROR: trying to open camera: %@", error);
	} else {
        [session addInput:input];
        
        stillImageOutput = [[AVCaptureStillImageOutput alloc] init];
        NSDictionary *outputSettings = [[NSDictionary alloc] initWithObjectsAndKeys: AVVideoCodecJPEG, AVVideoCodecKey, nil];
        [stillImageOutput setOutputSettings:outputSettings];
        
        [session addOutput:stillImageOutput];
        
        [session startRunning];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    [[segue destinationViewController] setHunt:hunt];
    if (![segue.identifier isEqualToString: @"winnerSegue"]) {
        [[segue destinationViewController] setSolutionImage:solutionPhoto.image];
        [[segue destinationViewController] setElapsedTime:timerLabel.text];
    }
}

- (void)setHunt:(id)newHunt
{
    if (hunt != newHunt) {
        hunt = newHunt;
        
        // Update the view.
        [self configureHunt];
    }
}

- (void)configureHunt
{
    // Update the user interface for the detail item.
    
    if (self.hunt) {
        destLoc = [[CLLocation alloc] initWithLatitude:[[hunt valueForKey:@"lat"] floatValue] longitude:[[hunt valueForKey:@"lon"] floatValue]];
    }
    
    huntImage = [UIImage imageWithData:[hunt valueForKey:@"blur"]];
    huntPhoto.image = huntImage;
    
    if ([[hunt valueForKey:@"blur"] isEqualToData:[hunt valueForKey:@"thumb"]]) {
        PFQuery *query = [PFQuery queryWithClassName:@"Hunt"];
        [query whereKey:@"huntID" equalTo:[hunt valueForKey:@"huntID"]];
        [query getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error){
            PFFile *blurFile = [object objectForKey:@"blur"];
            [blurFile getDataInBackgroundWithBlock:^(NSData *data, NSError *error){
                NSData *blurData = data;
                huntImage = [UIImage imageWithData:blurData];
                huntPhoto.image = huntImage;
            }];
            
        }];
    }
}

- (void) hideInstructions:(float)duration {
    UIView *instructionLabel = [self.view viewWithTag:100];
    
    [UIView animateWithDuration:duration delay:0.0
                        options: (UIViewAnimationOptionCurveEaseInOut & UIViewAnimationOptionBeginFromCurrentState)
                     animations:^{
                         instructionLabel.alpha = 0.0;
                     }
                     completion:^(BOOL complete){}];
}

- (void) showInstructions:(NSString *)text withDuration:(float)duration{
    UILabel *instructionLabel = (id)[self.view viewWithTag:100];
    instructionLabel.text = text;
    [UIView animateWithDuration:duration delay:0.0
                        options: (UIViewAnimationOptionCurveEaseInOut & UIViewAnimationOptionBeginFromCurrentState)
                     animations:^{
                         instructionLabel.alpha = 0.7;
                     }
                     completion:^(BOOL complete){
                         [NSTimer scheduledTimerWithTimeInterval:7.0 target:self selector:@selector(instructionTimerFired:) userInfo:nil repeats:NO];
                     }];
}

- (BOOL) inZone:(float)distance withAccuracy:(float)accuracy {
	if ((accuracy > distance) || (distance < MIN_DISTANCE)) {
        return YES;
	} else {
        return NO;
    }
}

- (float) getHeadingForDirectionFromCoordinate:(CLLocationCoordinate2D)fromLoc toCoordinate:(CLLocationCoordinate2D)toLoc
{
    float fLat = fromLoc.latitude * M_PI / 180;
    float fLon = fromLoc.longitude * M_PI / 180;
    float tLat = toLoc.latitude * M_PI / 180;
    float tLon = toLoc.longitude * M_PI / 180;
    
    float heading = (atan2(sin(tLon - fLon)*cos(tLat), cos(fLat)*sin(tLat)-sin(fLat)*cos(tLat)*cos(tLon - fLon))) * 180 / M_PI;
    return (heading > 0) ? heading : 360 + heading;
}

float getHeading (CLLocationCoordinate2D fromLoc, CLLocationCoordinate2D toLoc) {
    float fLat = fromLoc.latitude * M_PI / 180;
    float fLon = fromLoc.longitude * M_PI / 180;
    float tLat = toLoc.latitude * M_PI / 180;
    float tLon = toLoc.longitude * M_PI / 180;
    
    float heading = (atan2(sin(tLon - fLon)*cos(tLat), cos(fLat)*sin(tLat)-sin(fLat)*cos(tLat)*cos(tLon - fLon))) * 180 / M_PI;
    return (heading > 0) ? heading : 360 + heading;
}


- (UIBarButtonItem *)rightWinnersButton {
    if (!rightWinnersButton) {
        rightWinnersButton = [[UIBarButtonItem alloc] initWithTitle:@"see winners" style:UIBarButtonItemStyleBordered target:self action:@selector(seeWinners:)];
        //[rightWinnersButton setTintColor:[UIColor colorWithRed:87.0/255 green:87.0/255 blue:211.0/255 alpha:1.0]];
    }
    return rightWinnersButton;
}

- (UIBarButtonItem *)rightInviteButton {
    if (!rightInviteButton) {
        rightInviteButton = [[UIBarButtonItem alloc] initWithTitle:@"share" style:UIBarButtonItemStyleBordered target:self action:@selector(openMail:)];
        //[rightInviteButton setTintColor:[UIColor colorWithRed:87.0/255 green:87.0/255 blue:211.0/255 alpha:1.0]];
    }
    return rightInviteButton;
}

- (IBAction)openMail:(id)sender
{
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
        mailer.mailComposeDelegate = self;
        [mailer setSubject:@"Can you find this?"];
        NSData *imageData = [hunt valueForKey:@"thumb"];
        [mailer addAttachmentData:imageData mimeType:@"image/png" fileName:@"huntimage"];
        NSString *emailBody =[NSString stringWithFormat:@"I'm trying to solve a hunt on picseek. Can you find where this picture was taken?\n\n picseek://hunt/%@\n\nDon't have picseek yet? Get the free app here: https://itunes.apple.com/us/app/picseek/id591522053", [hunt valueForKey:@"huntID"]];
        [mailer setMessageBody:emailBody isHTML:NO];
        [self presentModalViewController:mailer animated:YES];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failure"
                                                        message:@"Your device doesn't support the composer sheet"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles: nil];
        [alert show];
    }
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled: you cancelled the operation and no email message was queued.");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved: you saved the email message in the drafts folder.");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail send: the email message is queued in the outbox. It is ready to send.");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail failed: the email message was not saved or queued, possibly due to an error.");
            break;
        default:
            NSLog(@"Mail not sent.");
            break;
    }
    // Remove the mail view
    [self dismissModalViewControllerAnimated:YES];
}

- (void)seeWinners:(id)sender {
    [self performSegueWithIdentifier:@"winnerSegue" sender:self];
}

- (void)goHome:(id)sender
{
    self.title = @"";
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(UILabel *)instructionLabel {
    CGRect viewRect = { 40.0, 90.0, 240.0, 140.0 };
    UILabel *instructionLabel = [[UILabel alloc] initWithFrame:viewRect];
    [instructionLabel setBackgroundColor:[UIColor blackColor]];
    [instructionLabel setTextColor:[UIColor whiteColor]];
    [instructionLabel setAlpha:0.0];
    [instructionLabel setNumberOfLines: 0];
    [instructionLabel setLineBreakMode: UILineBreakModeWordWrap];
    [instructionLabel setTextAlignment:UITextAlignmentCenter];
    [instructionLabel setFont:[UIFont boldSystemFontOfSize:18]];
    [instructionLabel setTag:100];
    
    return instructionLabel;
}

- (IBAction)handleTap:(UITapGestureRecognizer *)sender {
    [self hideInstructions:0];
}

- (IBAction)solveBtnClicked:(id)sender {
    [self showOverlay:sender];
}

- (IBAction)cancelBtnClicked:(id)sender {
    [self showOverlay:sender];
}

- (IBAction)clueBtnClicked:(id)sender {
    [self showClue];
}

-(void)timeChanged{
    NSTimeInterval now = [NSDate timeIntervalSinceReferenceDate];
    int seconds = (int)now - (int)startTime + (int)elapsedTime;
    
    timerLabel.text = [Helper prettyTime:seconds];
    
    if (timerLabel.hidden) {
        timerLabel.alpha = 0.0;
        timerLabel.hidden = NO;
        [UIView animateWithDuration:0.5 delay:0.5
                            options: (UIViewAnimationOptionCurveEaseInOut & UIViewAnimationOptionBeginFromCurrentState)
                         animations:^{
                             timerLabel.alpha = 0.5;
                         }
                         completion:^(BOOL complete){}];
    }
}

-(void)startTimer{
    if (!isSolved && ![timer isValid]) {
        startTime = [NSDate timeIntervalSinceReferenceDate];
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString *keyString = [NSString stringWithFormat:@"%@-elapsed", [hunt valueForKey:@"huntID"]];
        elapsedTime = [defaults floatForKey:keyString];
        NSLog(@"START TIMER -- ELAPSED FROM DEFAULTS: %f", elapsedTime);
        
        timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timeChanged) userInfo:nil repeats:YES];
    }
}

-(void)stopTimer{
    NSLog(@"STOP TIMER -- TIMER VALID: %@", [timer isValid]? @"YES": @"NO");
    if (!isSolved && [timer isValid]) {
        //record elapsed time
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString *keyString = [NSString stringWithFormat:@"%@-elapsed", [hunt valueForKey:@"huntID"]];
        NSTimeInterval now = [NSDate timeIntervalSinceReferenceDate];
        int seconds = (int)now - (int)startTime + (int)elapsedTime;
        [defaults setInteger:seconds forKey:keyString];
        NSLog(@"recorded time: %i, %i, %i, %i", (int)now, (int)startTime, (int)elapsedTime, seconds);
        [UIView animateWithDuration:0.5 delay:0.0
                            options: (UIViewAnimationOptionCurveEaseInOut & UIViewAnimationOptionBeginFromCurrentState)
                         animations:^{
                             timerLabel.alpha = 0.0;
                         }
                         completion:^(BOOL complete){timerLabel.hidden = YES;}];
    }
    
    [timer invalidate];
}

- (void)instructionTimerFired:(NSTimer *)instructionTimer {
    [self hideInstructions:0.5];
}

-(void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self stopTimer];
    [locationManager stopUpdatingLocation];
}

- (void) receiveBackgroundNotification:(NSNotification *) notification
{
    [self stopTimer];
}

- (void) receiveForegroundNotification:(NSNotification *) notification
{
    [self startTimer];
}

- (void)viewDidUnload
{
    [self stopTimer];
    [locationManager stopUpdatingLocation];
    
    [self setLocationManager:nil];
    [self setImagePreview:nil];
    [self setHuntPhoto:nil];
    [self setOverlayButtons:nil];
    [self setBtnSave:nil];
    [self setBtnRetake:nil];
    [self setTimerLabel:nil];
    [self setHideControls:nil];
    [self setSolutionPhoto:nil];
    [self setCompassView:nil];
    [self setCompass:nil];
    [self setDistLabel:nil];
    [self setClueAreaView:nil];
    [self setBtnCamera:nil];
    [self setBtnShowResults:nil];
    [self setDisplayAreaView:nil];
    [self setBtnCancel:nil];
    [self setBtnSolve:nil];
    [self setBtnSeeClue:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
@end
 
