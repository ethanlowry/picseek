//
//  Helper.m
//  picseek
//
//  Created by Ethan Lowry on 9/25/12.
//  Copyright (c) 2012 Ethan Lowry. All rights reserved.
//

#import "Helper.h"

#define MIN_DISTANCE 20

@implementation Helper

+ (NSString *)prettyDistance:(float)distance withAccuracy:(float)accuracy
{
    if (distance) {
        if ((accuracy > distance) || (distance < MIN_DISTANCE)) {
            return @"very close";
        } else {
            
            if (distance >= 10000) {
                distance = (int)(distance/10000)*10000;
                return [NSString stringWithFormat:@"%d kilometers",(int)(distance/1000)];
            } else if (distance >= 1000) {
                return [NSString stringWithFormat:@"%d kilometers",(int)round(distance/1000)];
            } else {
                distance = (int)(distance/10)*10;
                return [NSString stringWithFormat:@"%d meters",(int)distance];
            }
        }
    } else {
        return @"calculating distance...";
    }
}

+ (NSString *)prettyTime:(int)seconds
{
    NSString *dayText;
    dayText = (seconds > 86400) ? [NSString stringWithFormat:@"%dd ", (seconds / 86400)] : @"";
    return [NSString stringWithFormat:@"%@%02d:%02d:%02d", dayText, (seconds / 3600) % 24,(seconds / 60) % 60, seconds % 60];
}

+ (NSString *)username {
    NSString *username = nil;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults stringForKey:@"username"]) {
        username = [defaults stringForKey:@"username"];
        username = [username stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        if (username.length > 13) {
            username = [username substringWithRange:NSMakeRange(0, 13)];
        }
        [defaults setValue:username forKey:@"username"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    return username;
}


@end
