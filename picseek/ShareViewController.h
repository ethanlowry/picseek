//
//  ShareViewController.h
//  picseek
//
//  Created by Ethan Lowry on 10/15/12.
//  Copyright (c) 2012 Ethan Lowry. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
@interface ShareViewController : UIViewController <MFMailComposeViewControllerDelegate>

@property (strong, nonatomic) id hunt;
- (IBAction)openMail:(id)sender;
- (IBAction)goHome:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnInvite;
@property (weak, nonatomic) IBOutlet UIButton *btnDone;
@property (weak, nonatomic) IBOutlet UILabel *actionLabel;

@end
