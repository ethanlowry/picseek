//
//  HuntViewController.h
//  picseek
//
//  Created by Ethan Lowry on 9/7/12.
//  Copyright (c) 2012 Ethan Lowry. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import <QuartzCore/QuartzCore.h>
#import <ImageIO/ImageIO.h>
#import <CoreVideo/CoreVideo.h>
#import <CoreMedia/CoreMedia.h>
#import <CoreGraphics/CoreGraphics.h>

#import "UIImage+Resize.h"
#import <MessageUI/MessageUI.h>

@interface HuntViewController : UIViewController <CLLocationManagerDelegate, MFMailComposeViewControllerDelegate>



@property (weak, nonatomic) IBOutlet UIView *displayAreaView;
@property (weak, nonatomic) IBOutlet UIImageView *compass;
@property (strong, nonatomic) IBOutlet CLLocationManager *locationManager;
@property (strong, nonatomic) id hunt;
@property (weak, nonatomic) IBOutlet UIView *clueAreaView;
@property (strong, nonatomic) UIView *clueView;

@property (weak, nonatomic) IBOutlet UIImageView *huntPhoto;
@property (weak, nonatomic) IBOutlet UIImageView *solutionPhoto;

@property (weak, nonatomic) IBOutlet UIView *imagePreview;
@property(nonatomic, retain) AVCaptureStillImageOutput *stillImageOutput;

@property (weak, nonatomic) IBOutlet UILabel *distLabel;

@property (nonatomic, retain) UIBarButtonItem *rightWinnersButton;
@property (nonatomic, retain) UIBarButtonItem *rightInviteButton;

@property (weak, nonatomic) IBOutlet UIView *overlayButtons;
@property (weak, nonatomic) IBOutlet UIButton *btnSave;
@property (weak, nonatomic) IBOutlet UIButton *btnRetake;
@property (weak, nonatomic) IBOutlet UILabel *timerLabel;
@property (weak, nonatomic) IBOutlet UIView *hideControls;
@property (weak, nonatomic) IBOutlet UIButton *btnCamera;
@property (weak, nonatomic) IBOutlet UIButton *btnShowResults;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
@property (weak, nonatomic) IBOutlet UIButton *btnSolve;
@property (weak, nonatomic) IBOutlet UIButton *btnSeeClue;

@property (weak, nonatomic) IBOutlet UIView *compassView;

@property (nonatomic) BOOL showHomeButton;

- (IBAction)photoBtnClicked:(id)sender;
- (IBAction)saveBtnClicked:(id)sender;
- (IBAction)retakeBtnClicked:(id)sender;
- (IBAction)handleTap:(UITapGestureRecognizer *)sender;
- (IBAction)solveBtnClicked:(id)sender;
- (IBAction)cancelBtnClicked:(id)sender;
- (IBAction)clueBtnClicked:(id)sender;

- (float) getHeadingForDirectionFromCoordinate:(CLLocationCoordinate2D)fromLoc toCoordinate:(CLLocationCoordinate2D)toLoc;


@end
