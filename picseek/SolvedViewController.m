//
//  SolvedViewController.m
//  picseek
//
//  Created by Ethan Lowry on 9/25/12.
//  Copyright (c) 2012 Ethan Lowry. All rights reserved.
//

#import "SolvedViewController.h"
#import "WinnersViewController.h"
#import <Parse/Parse.h>

@interface SolvedViewController ()

@end


@implementation SolvedViewController

@synthesize elapsedTime;
@synthesize timeLabel;
@synthesize solutionImage;
@synthesize solutionPhoto;
@synthesize keyString;
@synthesize hunt;
@synthesize showHomeButton;

@synthesize btnReset;
@synthesize btnSeeWinners;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    if (showHomeButton) {
        UIBarButtonItem *homeButton = [[UIBarButtonItem alloc] initWithTitle:@"home" style:UIBarButtonItemStyleBordered target:self action:@selector(goHome:)];
        self.navigationItem.leftBarButtonItem = homeButton;
    }
    
    keyString = [hunt valueForKey:@"huntID"];
    
    timeLabel.text = elapsedTime; //[defaults valueForKey:keyString];
    solutionPhoto.image = solutionImage;
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"backgroundTile"]];
}

- (void)viewWillAppear:(BOOL)animated
{
    int buttonHeight = 95;
    int gap = 1;
    float screenHeight = self.view.frame.size.height;
    
    [solutionPhoto setFrame: CGRectMake(0,(screenHeight - buttonHeight - gap - 320)/2, 320, 320)];
    
    //button style
    [btnSeeWinners setBackgroundImage:[UIImage imageNamed:@"yellow"] forState:UIControlStateNormal];
    [btnSeeWinners setBackgroundImage:[UIImage imageNamed:@"yellowpressed"] forState:UIControlStateHighlighted];
    
    [btnReset setBackgroundImage:[UIImage imageNamed:@"purple"] forState:UIControlStateNormal];
    [btnReset setBackgroundImage:[UIImage imageNamed:@"purplepressed"] forState:UIControlStateHighlighted];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSArray *createdHunts = [defaults stringArrayForKey:@"createdHunts"];
    BOOL userCreated = [createdHunts containsObject:[hunt valueForKey:@"huntID"]];
    if (userCreated) {
        [btnSeeWinners setFrame:CGRectMake(107, screenHeight - buttonHeight, 213, buttonHeight)];
        [btnReset setFrame:CGRectMake(0, screenHeight - buttonHeight, 106, buttonHeight)];
        [btnReset setHidden:NO];
    } else {
        [btnSeeWinners setFrame:CGRectMake(0, screenHeight - buttonHeight, 320, buttonHeight)];
        [btnReset setHidden:YES];
    }
}

- (IBAction)resetBtnClicked:(id)sender {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    //remove the solved time, while preserving the initial start time
    [defaults removeObjectForKey:[NSString stringWithFormat:@"%@-solved", keyString]];
    [defaults removeObjectForKey:[NSString stringWithFormat:@"%@-solved.png", keyString]];
    [defaults removeObjectForKey:[NSString stringWithFormat:@"%@-message", keyString]];

    //check and remove any old solution by this device
    NSString *picseekUID = [defaults valueForKey:@"picseekUID"];
    PFQuery *query = [PFQuery queryWithClassName:@"Winner"];
    [query whereKey:@"picseekUID" equalTo:picseekUID];
    [query whereKey:@"huntID" equalTo:keyString];
    
    PFObject *pfOldWinner = [query getFirstObject];
    if (pfOldWinner) {
        [pfOldWinner deleteInBackground];
        //NSLog(@"YOU HAVE AN OLD WINNER (SVC): %@", [pfOldWinner objectForKey:@"huntID"]);
    }
    
    //decrement the winner count of this hunt
    query = [PFQuery queryWithClassName:@"Hunt"];
    [query whereKey:@"huntID" equalTo:[hunt valueForKey:@"huntID"]];
    PFObject *pfHunt = [query getFirstObject];
    int winnerCount = [[pfHunt valueForKey:@"winnerCount"] intValue];
    if(winnerCount && (winnerCount > 0)) {
        winnerCount = winnerCount - 1;
    } else {
        winnerCount = 0;
    }
    [pfHunt setValue:[NSNumber numberWithInt:winnerCount] forKey:@"winnerCount"];
    [pfHunt saveInBackground];

    //remove the saved image
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@-solved.png", keyString]];
    [fileManager removeItemAtPath: path error:NULL];
    
    //force save to defaults
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)goHome:(id)sender
{
    self.title = @"";
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    [[segue destinationViewController] setHunt:hunt];
}

/*
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
*/

- (void)viewDidUnload {
    [self setSolutionPhoto:nil];
    [self setTimeLabel:nil];
    [self setBtnSeeWinners:nil];
    [self setBtnReset:nil];
    [super viewDidUnload];
}

@end
