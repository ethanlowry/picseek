//
//  MessageViewController.h
//  picseek
//
//  Created by Ethan Lowry on 10/27/12.
//  Copyright (c) 2012 Ethan Lowry. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@interface MessageViewController : UIViewController
@property (weak, nonatomic) NSString *elapsedTime;
@property (weak, nonatomic) UIImage *solutionImage;
@property (weak, nonatomic) IBOutlet UIButton *btnSave;
@property (weak, nonatomic) IBOutlet UILabel *placeholderLabel;
@property (weak, nonatomic) IBOutlet UITextView *messageField;

@property (strong, nonatomic) id hunt;
- (IBAction)saveBtnClicked:(id)sender;

@end
