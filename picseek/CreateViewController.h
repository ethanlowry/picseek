//
//  CreateViewController.h
//  picseek
//
//  Created by Ethan Lowry on 9/7/12.
//  Copyright (c) 2012 Ethan Lowry. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import <CoreLocation/CoreLocation.h>
#import <CoreImage/CoreImage.h>

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import <QuartzCore/QuartzCore.h>
#import <ImageIO/ImageIO.h>
#import <CoreVideo/CoreVideo.h>
#import <CoreMedia/CoreMedia.h>

#import "UIImage+StackBlur.h"
#import "UIImage+Resize.h"

@interface CreateViewController : UIViewController <UINavigationControllerDelegate, UIImagePickerControllerDelegate, CLLocationManagerDelegate>

@property (strong, nonatomic) IBOutlet CLLocationManager *locationManager;
@property (weak, nonatomic) IBOutlet UIView *displayAreaView;

@property (weak, nonatomic) IBOutlet UIView *instructionsView;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (weak, nonatomic) IBOutlet UIImageView *selectedPhoto;
@property (weak, nonatomic) IBOutlet UIImageView *blurredPhoto;
@property (weak, nonatomic) IBOutlet UIImageView *cropShape;
@property (weak, nonatomic) IBOutlet UIView *imagePreview;
@property(nonatomic, retain) AVCaptureStillImageOutput *stillImageOutput;
@property (weak, nonatomic) IBOutlet UIButton *btnRetake;
@property (weak, nonatomic) IBOutlet UIButton *btnNext;
@property (weak, nonatomic) IBOutlet UIButton *btnCamera;
@property (weak, nonatomic) IBOutlet UIView *controlAreaView;

- (IBAction)photoBtnClicked:(id)sender;
- (IBAction)retakeBtnClicked:(id)sender;

- (IBAction)handlePan:(UIPanGestureRecognizer *)recognizer;
- (IBAction)handleTap:(UITapGestureRecognizer *)recognizer;
- (IBAction)handleCropPan:(UIPanGestureRecognizer *)recognizer;

@end
