//
//  Hunt.m
//  picseek
//
//  Created by Ethan Lowry on 11/8/12.
//  Copyright (c) 2012 Ethan Lowry. All rights reserved.
//

#import "Hunt.h"


@implementation Hunt

@dynamic blur;
@dynamic clue;
@dynamic dateCreated;
@dynamic distance;
@dynamic huntID;
@dynamic lat;
@dynamic locAccuracy;
@dynamic lon;
@dynamic photo;
@dynamic thumb;
@dynamic username;
@dynamic winnerCount;
@dynamic picseekUID;

@end
