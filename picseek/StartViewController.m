//
//  StartViewController.m
//  picseek
//
//  Created by Ethan Lowry on 9/7/12.
//  Copyright (c) 2012 Ethan Lowry. All rights reserved.
//

#import "StartViewController.h"
#import "AllHuntsListViewController.h"
#import "Helper.h"

@interface StartViewController ()

@end

@implementation StartViewController

@synthesize managedObjectContext;
@synthesize fetchedResultsController = _fetchedResultsController;
@synthesize btnCreate;
@synthesize btnCurrent;
@synthesize btnNearby;
@synthesize btnCreated;
@synthesize btnCompleted;
@synthesize nameView;
@synthesize firstTimeView;
@synthesize nameField;
@synthesize btnSave;
@synthesize nameInstructionLabel;


BOOL hasWinner = NO;
BOOL hasCurrent = NO;


- (IBAction)saveBtnClicked:(id)sender {
    // dismiss the keyboard
    [self.view endEditing:TRUE];
    
    // trim the string and save it to defaults
    NSString *username;
    username = [nameField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:username forKey:@"username"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    // animate the hiding of the firstTimeView.
    [UIView animateWithDuration:0.5 delay:0.0
                        options: (UIViewAnimationOptionCurveEaseInOut & UIViewAnimationOptionBeginFromCurrentState)
                     animations:^{
                         firstTimeView.frame = CGRectMake(0,self.view.frame.size.height, 320, self.view.frame.size.height);
                         firstTimeView.alpha = 0.0f;
                     }
                     completion:^(BOOL complete){
                         firstTimeView.hidden = YES;
                     }];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    NSString *result;
    result = [textField.text stringByReplacingCharactersInRange:range withString:string];
    result = [result stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if (textField.text.length == 0 && result.length == 0) {
        return NO;
    }
    
    if (result.length > 13) {
        return NO;
    }
    
    if (result.length > 0) {
        [btnSave setEnabled:YES];
    } else {
        [btnSave setEnabled:NO];
    }
    return YES;
}



-(void)viewWillAppear:(BOOL)animated
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSLog(@"NAME: %@",[Helper username]);
    // first time experience
    if (![Helper username]) {
        // layout
        int keyboardHeight = 216;
        int saveButtonHeight = 94;
        int gap = 2;
        int space = 15;
        int nameHeight = 38;
        int labelHeight = 20;
        float screenHeight = self.view.frame.size.height;
        
        [firstTimeView setFrame:CGRectMake(0,0,320, screenHeight)];
        [firstTimeView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"backgroundTile"]]];
        
        [nameView setFrame: CGRectMake(0,0, 320, screenHeight - saveButtonHeight - keyboardHeight - 2*gap)];
        [nameField setFrame: CGRectMake(20, (nameView.frame.size.height - space - nameHeight - labelHeight)/2, 280, nameHeight)];
        [nameField setDelegate:self];
        [nameInstructionLabel setFrame: CGRectMake(20, nameView.frame.size.height - space - labelHeight, 290, labelHeight)];
        
        [btnSave setFrame:CGRectMake(0, nameView.frame.size.height + gap, 320, saveButtonHeight)];
        [btnSave setTitleColor:[UIColor lightGrayColor] forState:UIControlStateDisabled];
        [btnSave setBackgroundImage:[UIImage imageNamed:@"blue"] forState:UIControlStateNormal];
        [btnSave setBackgroundImage:[UIImage imageNamed:@"bluepressed"] forState:UIControlStateHighlighted];
        [btnSave setTitleShadowColor:[UIColor clearColor] forState:UIControlStateDisabled];
        [btnSave setEnabled:NO];
        
        firstTimeView.hidden = NO;
        [nameField becomeFirstResponder];
    }
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navbar"] forBarMetrics:UIBarMetricsDefault];
    //Check for the presence of your hunts, current hunts and completed hunts
    NSMutableArray *buttonArray = [[NSMutableArray alloc] initWithObjects:btnCreate, btnCurrent, btnNearby, btnCreated, btnCompleted, nil];
    
    hasCurrent = hasWinner = NO;
    
    
    NSArray *defaultHunts = [defaults stringArrayForKey:@"currentHunts"];
    if (defaultHunts) {
        for (int i = 0; i < defaultHunts.count; i++) {
            if ([defaults objectForKey:[NSString stringWithFormat:@"%@-solved", [defaultHunts objectAtIndex:i]]]) {
                hasWinner = YES;
            } else {
                hasCurrent = YES;
            }
        }
    }
        if(!hasWinner) {
        //remove the "completed hunts" button
        [buttonArray removeObject:btnCompleted];
        btnCompleted.hidden = YES;
    }
    if(!hasCurrent) {
        //remove the "current hunts" button
        [buttonArray removeObject:btnCurrent];
        btnCurrent.hidden = YES;
    }
    
    //check for user created hunts
    NSError *error;
	if (![[self fetchedResultsController] performFetch:&error]) {
		// Update to handle the error appropriately.
		NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
		exit(-1);  // Fail
	}
    if ([_fetchedResultsController.fetchedObjects count] == 0) {
        //remove the "created hunts" button
        [buttonArray removeObject:btnCreated];
        btnCreated.hidden = YES;
    }
    
    int buttonWidth = self.view.bounds.size.width;
    int buttonHeight = round(self.view.bounds.size.height / buttonArray.count);
    
    for (int i = 0; i < buttonArray.count; i++) {
        UIButton *button = [buttonArray objectAtIndex:i];
        button.frame = CGRectMake(0, (i * buttonHeight), buttonWidth, buttonHeight);
        button.hidden = NO;
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [btnCreate setBackgroundImage:[UIImage imageNamed:@"green"] forState:UIControlStateNormal];
    [btnCurrent setBackgroundImage:[UIImage imageNamed:@"orange"] forState:UIControlStateNormal];
    [btnNearby setBackgroundImage:[UIImage imageNamed:@"pink"] forState:UIControlStateNormal];
    [btnCreated setBackgroundImage:[UIImage imageNamed:@"teal"] forState:UIControlStateNormal];
    [btnCompleted setBackgroundImage:[UIImage imageNamed:@"yellow"] forState:UIControlStateNormal];
    
    [btnCreate setBackgroundImage:[UIImage imageNamed:@"greenpressed"] forState:UIControlStateHighlighted];
    [btnCurrent setBackgroundImage:[UIImage imageNamed:@"orangepressed"] forState:UIControlStateHighlighted];
    [btnNearby setBackgroundImage:[UIImage imageNamed:@"pinkpressed"] forState:UIControlStateHighlighted];
    [btnCreated setBackgroundImage:[UIImage imageNamed:@"tealpressed"] forState:UIControlStateHighlighted];
    [btnCompleted setBackgroundImage:[UIImage imageNamed:@"yellowpressed"] forState:UIControlStateHighlighted];

}

- (void)viewDidUnload
{
    [self setBtnCreate:nil];
    [self setBtnCurrent:nil];
    [self setBtnNearby:nil];
    [self setBtnCreated:nil];
    [self setBtnCompleted:nil];
    [self setNameView:nil];
    [self setFirstTimeView:nil];
    [self setNameField:nil];
    [self setBtnSave:nil];
    [self setNameInstructionLabel:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    //rollback any uncommitted changes to the managed object
    if([[segue identifier] isEqualToString:@"create hunt"] || [[segue identifier] isEqualToString:@"your hunts"]) {
        [managedObjectContext rollback];
        [[segue destinationViewController] setManagedObjectContext:self.managedObjectContext];
    } else {
        UIButton* senderButton = (UIButton*)sender;
        AllHuntsListViewController *ahlvc = [segue destinationViewController];
        
        if (senderButton.tag == 999) {
            [ahlvc setShowCurrent:YES];
            [ahlvc setTitle:@"current hunts"];
        } else if (senderButton.tag == 998) {
            [ahlvc setShowWinners:YES];
            [ahlvc setTitle:@"victories"];

        } else {
            [ahlvc setTitle:@"nearby hunts"];
        }
    }
}


- (NSFetchedResultsController *)fetchedResultsController {
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Hunt" inManagedObjectContext:managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSSortDescriptor *sort = [[NSSortDescriptor alloc] initWithKey:@"distance" ascending:YES];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObject:sort]];
    
    [fetchRequest setFetchBatchSize:1];
    NSFetchedResultsController *theFetchedResultsController =
    [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                        managedObjectContext:managedObjectContext sectionNameKeyPath:nil
                                                   cacheName:@"Root"];
    self.fetchedResultsController = theFetchedResultsController;
    _fetchedResultsController.delegate = self;
    return _fetchedResultsController;
}

@end
