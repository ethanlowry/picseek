//
//  AllHuntsListViewController.h
//  picseek
//
//  Created by Ethan Lowry on 9/17/12.
//  Copyright (c) 2012 Ethan Lowry. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface AllHuntsListViewController : UITableViewController <CLLocationManagerDelegate>
@property (strong, nonatomic) IBOutlet CLLocationManager *locationManager;
@property (strong, nonatomic) CLLocation *currentLocation;
@property (nonatomic) BOOL showCurrent;
@property (nonatomic) BOOL showWinners;
@property (nonatomic) BOOL reloadData;

@end
