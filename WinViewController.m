//
//  WinViewController.m
//  picseek
//
//  Created by Ethan Lowry on 10/27/12.
//  Copyright (c) 2012 Ethan Lowry. All rights reserved.
//

#import "WinViewController.h"
#import <Parse/Parse.h>

@interface WinViewController ()

@end

@implementation WinViewController

@synthesize winnerPhoto;
@synthesize messageAreaView;
@synthesize messageText;
@synthesize winner;


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    messageAreaView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"backgroundTile"]];
    messageText.text = [winner valueForKey:@"message"];
    
    PFFile *photoFile = [winner objectForKey:@"photo"];
    NSData *photoData = [photoFile getData];
    winnerPhoto.image = [UIImage imageWithData:photoData];
    
    self.title = [winner valueForKey:@"username"];
}

- (void)viewWillAppear:(BOOL)animated
{
    int messageHeight = 96;
    float screenHeight = self.view.frame.size.height;
    
    [winnerPhoto setFrame: CGRectMake(0,(screenHeight - messageHeight - 320)/2, 320, 320)];
    
    [messageAreaView setFrame:CGRectMake(0, screenHeight - messageHeight, 320, messageHeight)];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setWinnerPhoto:nil];
    [self setMessageAreaView:nil];
    [self setMessageText:nil];
    [super viewDidUnload];
}
@end
