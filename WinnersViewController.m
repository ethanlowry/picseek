//
//  WinnersViewController.m
//  picseek
//
//  Created by Ethan Lowry on 10/19/12.
//  Copyright (c) 2012 Ethan Lowry. All rights reserved.
//

#import "WinnersViewController.h"
#import "WinViewController.h"
#import <Parse/Parse.h>

@interface WinnersViewController ()<UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, strong) UITableView *tableView;
- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath;
@end

@implementation WinnersViewController
@synthesize tableView = _tableView;
@synthesize hunt;
@synthesize winnersView;
@synthesize buttonArea;
@synthesize btnInvite;
@synthesize userCreated;

NSMutableArray *winners;


- (void)viewWillAppear:(BOOL)animated
{
    int buttonHeight = 95;
    int gap = 1;
    float screenHeight = self.view.frame.size.height;
    
    [winnersView setFrame: CGRectMake(0,0, 320, screenHeight - buttonHeight - gap)];
    [buttonArea setFrame: CGRectMake(0,winnersView.frame.size.height, 320, buttonHeight + gap)];
    
    [btnInvite setFrame:CGRectMake(0, gap, 320, buttonHeight)];
    [btnInvite setBackgroundImage:[UIImage imageNamed:@"orange"] forState:UIControlStateNormal];
    [btnInvite setBackgroundImage:[UIImage imageNamed:@"orangepressed"] forState:UIControlStateHighlighted];
    
    //create empty label
    UILabel *emptyLabel = [[UILabel alloc] initWithFrame:winnersView.frame];
    [emptyLabel setBackgroundColor:[UIColor whiteColor]];
    [emptyLabel setAlpha:0];
    [emptyLabel setTextColor:[UIColor darkTextColor]];
    [emptyLabel setTextAlignment:UITextAlignmentCenter];
    [emptyLabel setFont:[UIFont boldSystemFontOfSize:18]];
    [emptyLabel setText:@"No one's solved it yet!"];
    [emptyLabel setTag:101];
    [winnersView addSubview:emptyLabel];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    buttonArea.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"backgroundTile"]];
    
    // determine if this is a hunt the user created
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSArray *createdHunts = [defaults stringArrayForKey:@"createdHunts"];
    userCreated = [createdHunts containsObject:[hunt valueForKey:@"huntID"]];

    // create the winners table
    self.tableView = [[UITableView alloc] initWithFrame:winnersView.bounds style:UITableViewStylePlain];
    _tableView.dataSource = self;
    _tableView.delegate = self;
    [winnersView addSubview:self.tableView];
    winners = nil;
    [self.tableView reloadData];
    
    PFQuery *query = [PFQuery queryWithClassName:@"Winner"];
    [query whereKey:@"huntID" equalTo:[hunt valueForKey:@"huntID"]];
    [query orderByDescending:@"createdAt"];
    
    //NSInteger limit = 10;
    //query.limit = limit;
    
    //create loading label
    CGRect viewRect = { 20.0, ((winnersView.frame.size.height-60)/2), 280.0, 60.0 };
    UILabel *loadingLabel = [[UILabel alloc] initWithFrame:viewRect];
    [loadingLabel setBackgroundColor:[UIColor blackColor]];
    [loadingLabel setAlpha:.7];
    [loadingLabel setTextColor:[UIColor whiteColor]];
    [loadingLabel setTextAlignment:UITextAlignmentCenter];
    [loadingLabel setFont:[UIFont boldSystemFontOfSize:18]];
    [loadingLabel setText:@"Downloading winners"];
    [loadingLabel setTag:100];
    [winnersView addSubview:loadingLabel];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            // The find succeeded.
            
            winners = [[NSMutableArray alloc] initWithArray:objects];

            //hide loading label
            [UIView beginAnimations:nil context:NULL];
            [UIView setAnimationDuration:0.5f];
            [self.view viewWithTag:100].alpha = 0.0f; //this is the "loadingLabel"
            [UIView commitAnimations];
            
            if (winners.count > 0) {
                [self.tableView reloadData];
            } else {
                [UIView animateWithDuration:0.5 delay:0.0
                                    options: (UIViewAnimationOptionCurveEaseInOut & UIViewAnimationOptionBeginFromCurrentState)
                                 animations:^{
                                     [self.view viewWithTag:101].alpha = 1.0;
                                 }
                                 completion:^(BOOL complete){}];
            }
            
            //correct the hunt's saved winner count if it's wrong
            if (winners.count != [[hunt valueForKey:@"winnerCount"] intValue]){
                PFQuery *huntQuery = [PFQuery queryWithClassName:@"Hunt"];
                [huntQuery whereKey:@"huntID" equalTo:[hunt valueForKey:@"huntID"]];
                [huntQuery getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error){
                    [object setValue:[NSNumber numberWithInt:winners.count] forKey:@"winnerCount"];
                    [object saveInBackground];
                }];
            }

        } else {
            // Log details of the failure
            NSLog(@"Error: %@ %@", error, [error userInfo]);
        }
    }];

}

- (IBAction)openMail:(id)sender
{
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
        mailer.mailComposeDelegate = self;
        [mailer setSubject:@"Can you find this?"];
        NSData *imageData = [hunt valueForKey:@"thumb"];
        [mailer addAttachmentData:imageData mimeType:@"image/png" fileName:@"huntimage"];
        
        NSString *emailBody;
        if (userCreated){
            emailBody =[NSString stringWithFormat:@"I've set up a hunt on picseek. Can you find where I took this picture?\n\n picseek://hunt/%@ \n\nDon't have picseek yet? Get the free app here: https://itunes.apple.com/us/app/picseek/id591522053", [hunt valueForKey:@"huntID"]];
        } else {
            emailBody =[NSString stringWithFormat:@"I've just solved a hunt on picseek. Can you find where this picture was taken?\n\n picseek://hunt/%@ \n\nDon't have picseek yet? Get the free app here: https://itunes.apple.com/us/app/picseek/id591522053", [hunt valueForKey:@"huntID"]];
        }
        [mailer setMessageBody:emailBody isHTML:NO];
        [self presentModalViewController:mailer animated:YES];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failure"
                                                        message:@"Your device doesn't support the composer sheet"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles: nil];
        [alert show];
    }
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled: you cancelled the operation and no email message was queued.");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved: you saved the email message in the drafts folder.");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail send: the email message is queued in the outbox. It is ready to send.");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail failed: the email message was not saved or queued, possibly due to an error.");
            break;
        default:
            NSLog(@"Mail not sent.");
            break;
    }
    // Remove the mail view
    [self dismissModalViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setWinnersView:nil];
    [self setButtonArea:nil];
    [self setBtnInvite:nil];
    [super viewDidUnload];
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    if (winners.count > 0) {
        id winner = [winners objectAtIndex:indexPath.row];
        cell.textLabel.lineBreakMode = UILineBreakModeWordWrap;
        cell.textLabel.numberOfLines = 3;
        cell.textLabel.text = [winner valueForKey:@"message"];
        
        cell.detailTextLabel.lineBreakMode = UILineBreakModeWordWrap;
        cell.detailTextLabel.numberOfLines = 2;
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%@\ntime: %@",[winner valueForKey:@"username"], [winner valueForKey:@"time"]];

        UIImage *image = [UIImage imageWithData:[winner valueForKey:@"thumb"]];

        cell.imageView.image = image;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    [[segue destinationViewController] setWinner:[winners objectAtIndex:indexPath.row]];
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - UITableViewDataSource Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [winners count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell...
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100;
}



#pragma mark - UITableViewDelegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:@"winSegue" sender:self];
}

@end
