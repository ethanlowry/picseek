//
//  WinViewController.h
//  picseek
//
//  Created by Ethan Lowry on 10/27/12.
//  Copyright (c) 2012 Ethan Lowry. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WinViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *winnerPhoto;
@property (weak, nonatomic) IBOutlet UIView *messageAreaView;
@property (weak, nonatomic) IBOutlet UITextView *messageText;
@property (strong, nonatomic) id winner;

@end
