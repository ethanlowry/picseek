//
//  AllHuntsListViewController.m
//  picseek
//
//  Created by Ethan Lowry on 9/17/12.
//  Copyright (c) 2012 Ethan Lowry. All rights reserved.
//

#import <Parse/Parse.h>
#import "Hunt.h"
#import "AllHuntsListViewController.h"
#import "HuntViewController.h"
#import "Helper.h"

@interface AllHuntsListViewController ()
- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath;
@end

@implementation AllHuntsListViewController
@synthesize locationManager;
@synthesize currentLocation;
@synthesize showCurrent;
@synthesize showWinners;
@synthesize reloadData;

BOOL queryNeeded = NO;

NSMutableArray *hunts;
NSMutableArray *currentHunts;
NSMutableArray *nearbyHunts;
NSMutableArray *completedHunts;

int currentHuntsCount;
int completedHuntsCount;

int page;
int limit = 10;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // clear hunts and start location manager
    page = 0;
    hunts = nil;
    hunts = [[NSMutableArray alloc] init];
    currentLocation = nil;
    locationManager.delegate = self;
    [locationManager startUpdatingLocation];
    
    // ui setup
    UIBarButtonItem *homeButton = [[UIBarButtonItem alloc] initWithTitle:@"home" style:UIBarButtonItemStyleBordered target:self action:@selector(goHome:)];
    self.navigationItem.leftBarButtonItem = homeButton;
    
    NSString *navBackground;
    if (showCurrent) {
        navBackground = @"orangenav";
    } else if (showWinners) {
        navBackground = @"yellownav";
    } else {
        navBackground = @"pinknav";
    }
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:navBackground] forBarMetrics:UIBarMetricsDefault];
    
    if (showCurrent) {
        // show hunts the user is currently trying to solve
        if (currentHunts) {
            // if we have a currentHunts list, use the first page's worth of it
            if (currentHunts.count > limit) {
                currentHunts = [[NSMutableArray alloc] initWithArray:[currentHunts subarrayWithRange:NSMakeRange(0, limit)]];
            }
            hunts = currentHunts;
            // compare count of goodhunts to the last saved number in currentHuntsCount, and only execute the query if they are different
            NSMutableArray *goodHunts = [self goodHunts];
            if (currentHuntsCount != goodHunts.count) {
                queryNeeded = YES;
            }
        } else {
            //if not, turn on the loading label and run the query
            [self.view addSubview:self.loadingLabel];
            queryNeeded = YES;
        }       
    } else if (showWinners) {
        // show hunts the user has completed
        if (completedHunts) {
            // if we have a completedHunts list, use the first page's worth of it
            if (completedHunts.count > limit) {
                completedHunts = [[NSMutableArray alloc] initWithArray:[completedHunts subarrayWithRange:NSMakeRange(0, limit)]];
            }
            hunts = completedHunts;
    
            // compare count of solvedHunts to the last saved number in currentHuntsCount, and only execute the query if they are different
            NSMutableArray *solvedHunts = [self solvedHunts];
            if (completedHuntsCount != solvedHunts.count) {
                queryNeeded = YES;
            }

        } else {
            //if not, turn on the loading label and run the query
            [self.view addSubview:self.loadingLabel];
            queryNeeded = YES;
        }
    } else {
        // show nearby hunts
        if (nearbyHunts) {
            // if we have a nearbytHunts list, use the first page's worth of it
            if (nearbyHunts.count > limit){
                nearbyHunts = [[NSMutableArray alloc] initWithArray:[nearbyHunts subarrayWithRange:NSMakeRange(0, limit)]];
            }
            hunts = nearbyHunts;
        } else {
            [self.view addSubview:self.loadingLabel];
        }
        queryNeeded = YES;
    }
    if (queryNeeded) {
        [self runQuery:NO];
    }
    
    [self.tableView reloadData];
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row < hunts.count) {
        if ([cell.contentView viewWithTag:101]) {
            [[cell.contentView viewWithTag:101]removeFromSuperview];
        }
        Hunt *thisHunt = [hunts objectAtIndex:indexPath.row];
        
        NSString *clueText = [thisHunt valueForKey:@"clue"];
        cell.textLabel.text = [NSString stringWithFormat:@"%@ ",clueText];
        cell.imageView.contentMode = UIViewContentModeScaleAspectFit;
        
        // the image is tricky because thumbs load in lazily.
        if ([[thisHunt valueForKey:@"imageReady"] isEqualToString: @"ready"]) {
            UIImage *image = [UIImage imageWithData:[thisHunt valueForKey:@"thumb"]];
            cell.imageView.image = image;
        } else {
            UIImage *image = [UIImage imageNamed:@"blankthumb"];
            cell.imageView.image = image;
        }

        float totalAccuracy = currentLocation.horizontalAccuracy;
        if([thisHunt valueForKey:@"locAccuracy"]) {
            totalAccuracy = totalAccuracy + [[thisHunt valueForKey:@"locAccuracy"] floatValue];
        }
        
        cell.detailTextLabel.lineBreakMode = UILineBreakModeWordWrap;
        cell.detailTextLabel.numberOfLines = 3;
        NSString *detailText = [NSString stringWithFormat:@"by %@\n%@", [thisHunt valueForKey:@"username"], [Helper prettyDistance:[[thisHunt valueForKey:@"distance"] floatValue] withAccuracy:totalAccuracy]];
        
        int winnerCount = [[thisHunt valueForKey:@"winnerCount"] intValue];
        if (winnerCount && (winnerCount > 0)) {
            cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ \n ", detailText];
            if(![cell viewWithTag:999]) {
                [self addWinnerLabel:cell withCount:winnerCount];
            }
        } else {
            cell.detailTextLabel.text = detailText;
            if([cell viewWithTag:999]) {
                [[cell viewWithTag:999] removeFromSuperview];
            }
        }
     } else if (indexPath.row == hunts.count){
        // show pagination button
        cell.imageView.image = nil;
        cell.textLabel.text = nil;
        cell.detailTextLabel.text = nil;
        cell.accessoryView = nil;
        cell.accessoryType = UITableViewCellAccessoryNone;
        if([cell viewWithTag:999]) {
            [[cell viewWithTag:999] removeFromSuperview];
        }
        if (![cell viewWithTag:101]) {
            UIButton *btnNext = [self btnNext];
            btnNext.alpha = 0;
            [cell.contentView addSubview:btnNext];
            [UIView animateWithDuration:0.5 delay:0.0
                                options: (UIViewAnimationOptionCurveEaseInOut & UIViewAnimationOptionBeginFromCurrentState)
                             animations:^{
                                 btnNext.alpha = 1.0f;
                             }
                             completion:^(BOOL complete){}];
        }
    } 
}

- (void)populateHunts:(NSArray *)objects withLocation:(BOOL)showLocation withPictures:(BOOL)showPictures withStartIndex:(int)startIndex
{
    for (int i = 0; i < objects.count; i++) {
        PFObject *eachObject = [objects objectAtIndex:i];
        int huntIndex = startIndex + i;
        
        if (showPictures) {
            PFFile *thumbFile = [eachObject objectForKey:@"thumb"];
            //temporarily set the thumb image to a blank image, while data is being loaded
            NSData *tempData = UIImagePNGRepresentation([UIImage imageNamed:@"blankthumb"]);
            
            // the page number can get ahead of downloading. prevent attempts to update hunts that don't exist yet.
            if (huntIndex < [hunts count]){
                [[hunts objectAtIndex:huntIndex] setValue:tempData forKey:@"thumb"];
                [[hunts objectAtIndex:huntIndex] setValue:tempData forKey:@"blur"];
                
                [thumbFile getDataInBackgroundWithBlock:^(NSData *data, NSError *error){
                    [[hunts objectAtIndex:huntIndex] setValue:data forKey:@"thumb"];
                    //temporarily set the full size image = to the thumb image
                    [[hunts objectAtIndex:huntIndex] setValue:data forKey:@"blur"];
                    [[hunts objectAtIndex:huntIndex] setValue:@"ready" forKey:@"imageReady"];
                    // THIS FEELS INEFFICIENT TO DO FOR EACH ROW, BUT WE NEED TO GET THE NEW PIC REFLECTED IN THE TABLE
                    //[self.tableView reloadData];
                    NSIndexPath *loadedPath = [NSIndexPath indexPathForRow:huntIndex inSection:0];
                    [self.tableView reloadRowsAtIndexPaths:@[loadedPath] withRowAnimation:UITableViewRowAnimationNone];
                }];
            }
            
        }
        if (showLocation) {
            if (currentLocation != nil) {
                CLLocation *destLoc = [[CLLocation alloc] initWithLatitude:[[eachObject valueForKey:@"lat"] floatValue] longitude:[[eachObject valueForKey:@"lon"] floatValue]];
                
                float distance = [currentLocation distanceFromLocation:destLoc];
                // the page number can get ahead of downloading. prevent attempts to update hunts that don't exist yet.
                if (huntIndex < [hunts count]){
                    [[hunts objectAtIndex:huntIndex] setValue:[NSNumber numberWithFloat:distance] forKey:@"distance"];
                }
            }
        }
    }
}

- (void) runQuery:(BOOL)withMoreHunts {
    if (!currentLocation) {
        UILabel *loadingLabel = (id)[self.view viewWithTag:100];
        loadingLabel.text = @"Finding your location";
    }
    if (currentLocation && queryNeeded){
        UILabel *loadingLabel = (id)[self.view viewWithTag:100];
        loadingLabel.text = @"Downloading hunts";
        queryNeeded = NO;
        PFQuery *query = [PFQuery queryWithClassName:@"Hunt"];

        query.limit = limit;
        query.skip = limit*page;
        
        if (showCurrent) {
            // GET PARSE DATA FOR CURRENT IN-PROGRESS HUNTS
            
            // record the current number of completed hunts
            NSMutableArray *goodHunts = [self goodHunts];
            completedHuntsCount = goodHunts.count;

            [query whereKey:@"location" nearGeoPoint:[PFGeoPoint geoPointWithLatitude:currentLocation.coordinate.latitude  longitude:currentLocation.coordinate.longitude]];
            [query whereKey:@"huntID" containedIn:goodHunts];
            [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
                if (!error) {
                    // The find succeeded.
                    if (withMoreHunts) {
                        [hunts addObjectsFromArray:objects];
                    } else {
                        hunts = [[NSMutableArray alloc] initWithArray:objects];
                    }
                    [self populateHunts:objects withLocation:YES withPictures:NO withStartIndex:page*limit];
                    currentHunts = hunts;
                    [self hideLoadingLabel];
                    [self.tableView reloadData];
                    [self populateHunts:objects withLocation:NO withPictures:YES withStartIndex:page*limit];
                    
                } else {
                    // Log details of the failure
                    NSLog(@"Error: %@ %@", error, [error userInfo]);
                }
            }];
        } else if (showWinners) {
            // GET PARSE DATA FOR COMPLETED HUNTS
            
            // record the current number of completed hunts
            NSMutableArray *solvedHunts = [self solvedHunts];
            completedHuntsCount = solvedHunts.count;
 
            [query whereKey:@"location" nearGeoPoint:[PFGeoPoint geoPointWithLatitude:currentLocation.coordinate.latitude  longitude:currentLocation.coordinate.longitude]];
            [query whereKey:@"huntID" containedIn:solvedHunts];
            [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
                if (!error) {
                    // The find succeeded.
                    if (withMoreHunts) {
                        [hunts addObjectsFromArray:objects];
                    } else {
                        hunts = [[NSMutableArray alloc] initWithArray:objects];
                    }
                    [self populateHunts:objects withLocation:YES withPictures:NO withStartIndex:page*limit];
                    completedHunts = hunts;
                    [self hideLoadingLabel];
                    [self.tableView reloadData];
                    [self populateHunts:objects withLocation:NO withPictures:YES withStartIndex:page*limit];

                } else {
                    // Log details of the failure
                    NSLog(@"Error: %@ %@", error, [error userInfo]);
                }
            }];
        } else {
            // GET PARSE DATA FOR NEARBY HUNTS
            [query whereKey:@"location" nearGeoPoint:[PFGeoPoint geoPointWithLatitude:currentLocation.coordinate.latitude  longitude:currentLocation.coordinate.longitude] withinMiles:20];
            [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
                if (!error) {
                    // The find succeeded.
                    if (withMoreHunts) {
                        [hunts addObjectsFromArray:objects];
                    } else {
                        hunts = [[NSMutableArray alloc] initWithArray:objects];
                    }
                    if (hunts.count == 0) {
                        loadingLabel.text = @"Alas, no hunts nearby.";
                    } else {
                        [self populateHunts:objects withLocation:YES withPictures:NO withStartIndex:page*limit];
                        nearbyHunts = hunts;
                        [self hideLoadingLabel];
                        [self.tableView reloadData];
                        [self populateHunts:objects withLocation:NO withPictures:YES withStartIndex:page*limit];
                    }
                } else {
                    // Log details of the failure
                    NSLog(@"Error: %@ %@", error, [error userInfo]);
                }
            }];
        }
    }
}

// ---------- CLLocationManager delegate functions ---------------

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    currentLocation = newLocation;
    
    if (currentLocation.horizontalAccuracy < 20) {
        [manager stopUpdatingLocation];
    }
    [self runQuery:NO];
    
    if (hunts) {
        [self populateHunts:hunts withLocation:YES withPictures:NO withStartIndex:0];
        [self.tableView reloadData];
    }
}


- (void)goHome:(id)sender
{
    self.title = @"";
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (NSMutableArray *)goodHunts {
    // determine which hunts are still in progress
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSArray *defaultHunts = [[NSArray alloc] initWithArray:[defaults stringArrayForKey:@"currentHunts"]];
    NSMutableArray *goodHunts = [[NSMutableArray alloc] init];
    int count = defaultHunts.count;
    for (int i = 0; i < count; i++) {
        if (![defaults objectForKey:[NSString stringWithFormat:@"%@-solved", [defaultHunts objectAtIndex:i]]]) {
            [goodHunts addObject:[defaultHunts objectAtIndex:i]];
        }
    }
    return goodHunts;
}

- (NSMutableArray *)solvedHunts {
    // determine which hunts have been solved
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSArray *defaultHunts = [[NSArray alloc] initWithArray:[defaults stringArrayForKey:@"currentHunts"]];
    NSMutableArray *solvedHunts = [[NSMutableArray alloc] init];
    int count = defaultHunts.count;
    for (int i = 0; i < count; i++) {
        if ([defaults objectForKey:[NSString stringWithFormat:@"%@-solved", [defaultHunts objectAtIndex:i]]]) {
            [solvedHunts addObject:[defaultHunts objectAtIndex:i]];
        }
    }
    return solvedHunts;
}

- (UIButton *)btnNext {
    CGRect buttonRect = { 0,0,320,100 };
    UIButton *btnNext = [[UIButton alloc] initWithFrame:buttonRect];
    [btnNext setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnNext setTitle:@"more" forState:UIControlStateNormal];
    btnNext.titleLabel.font = [UIFont boldSystemFontOfSize:28];
    [btnNext setTag:101];
    [btnNext setBackgroundImage:[UIImage imageNamed:@"blue"] forState:UIControlStateNormal];
    [btnNext setBackgroundImage:[UIImage imageNamed:@"bluepressed"] forState:UIControlStateHighlighted];
    
    [btnNext addTarget:self
               action:@selector(getMoreHunts:)
     forControlEvents:UIControlEventTouchUpInside];
    
    return btnNext;
}

- (void)getMoreHunts:(id)sender
{
    // PROBLEM: there's a race condition here. If the page number has been incrememnted, but the additional hunts haven't loaded yet
    page = page + 1;
    
    queryNeeded = YES;
    [self runQuery:YES];
    UIButton *btnNext = sender;
    [UIView animateWithDuration:0.5 delay:0.0
                        options: (UIViewAnimationOptionCurveEaseInOut & UIViewAnimationOptionBeginFromCurrentState)
                     animations:^{
                         btnNext.alpha = 0.0f;
                     }
                     completion:^(BOOL complete){}];
}

- (UILabel *)loadingLabel {
    if (![self.view viewWithTag:100]) {
        //loading label
        CGRect viewRect = { 20.0, 180.0, 280.0, 60.0 };
        UILabel *loadingLabel = [[UILabel alloc] initWithFrame:viewRect];
        [loadingLabel setBackgroundColor:[UIColor blackColor]];
        [loadingLabel setAlpha:.7];
        [loadingLabel setTextColor:[UIColor whiteColor]];
        [loadingLabel setTextAlignment:UITextAlignmentCenter];
        [loadingLabel setFont:[UIFont boldSystemFontOfSize:18]];
        [loadingLabel setText:@"Downloading hunts"];
        [loadingLabel setTag:100];
        return loadingLabel;
    } else {
        UILabel *loadingLabel = (id)[self.view viewWithTag:100];
        return loadingLabel;
    }
}

- (void)hideLoadingLabel
{
    [UIView animateWithDuration:0.5 delay:0.0
                        options: (UIViewAnimationOptionCurveEaseInOut & UIViewAnimationOptionBeginFromCurrentState)
                     animations:^{
                         [self.view viewWithTag:100].alpha = 0.0f; //this is the "loadingLabel"
                     }
                     completion:^(BOOL complete){}];
}

- (void)addWinnerLabel:(UITableViewCell *)cell withCount:(int)count
{    
    //add the winner label
    UILabel *winnerLabel = [[UILabel alloc] init];
    winnerLabel.tag = 999;
    float winnerHeight = 18;
    float winnerX = 109;
    float winnerTop = 70;
    float winnerWidth = 200;
    
    winnerLabel.frame = CGRectMake(winnerX, winnerTop, winnerWidth, winnerHeight);
    winnerLabel.font = cell.detailTextLabel.font;
    winnerLabel.textColor = [UIColor orangeColor];
    winnerLabel.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.0];
    winnerLabel.text = [NSString stringWithFormat:@"winners: %d", count];
    [cell addSubview:winnerLabel];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    Hunt *thisHunt = [hunts objectAtIndex:indexPath.row];
    
    [[segue destinationViewController] setHunt:thisHunt];
}

- (void)viewWillAppear:(BOOL)animated
{
    [locationManager startUpdatingLocation];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [locationManager stopUpdatingLocation];
    [[self.view viewWithTag:100] removeFromSuperview];
}

- (void)viewDidUnload
{
    [self setLocationManager:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if (hunts.count > 0 && hunts.count%limit == 0) {
        return ([hunts count] + 1);
    }
    return [hunts count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    // Configure the cell...
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:@"huntSegue" sender:self];

}

@end