//
//  WinnersViewController.h
//  picseek
//
//  Created by Ethan Lowry on 10/19/12.
//  Copyright (c) 2012 Ethan Lowry. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@interface WinnersViewController : UIViewController  <MFMailComposeViewControllerDelegate>
@property (strong, nonatomic) id hunt;
@property (weak, nonatomic) IBOutlet UIView *winnersView;
@property (weak, nonatomic) IBOutlet UIView *buttonArea;
@property (weak, nonatomic) IBOutlet UIButton *btnInvite;
@property (nonatomic) BOOL userCreated;
- (IBAction)openMail:(id)sender;

@end
